package com.bulikeji.widget;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bulikeji.R;

/**
 * Created by Administrator on 2017/3/18 0018.
 */

public class TitleBarView extends LinearLayout {

    private Context context;
    private View inflate;
    private TextView back;
    private TextView title;
    private TextView titleR;

    public TitleBarView(Context context) {
        super(context);
    }

    public TitleBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    private void init() {
        inflate = View.inflate(context, R.layout.titlebar_view, this);
        back = (TextView) inflate.findViewById(R.id.back);
        title = (TextView) inflate.findViewById(R.id.title);
        titleR = (TextView) inflate.findViewById(R.id.title_r);
        back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Activity) context).finish();
            }
        });
    }

    public void hideBack() {
        back.setVisibility(GONE);
    }

    public void setTitle(String titles) {
        title.setText(titles);
    }

    public void setTitleR(String title, OnClickListener OnClickListener) {
        this.titleR.setText(title);
        this.titleR.setOnClickListener(OnClickListener);
    }
}
