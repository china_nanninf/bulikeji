package com.bulikeji.widget;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bulikeji.R;

/**
 * Created by Administrator on 2017/3/18 0018.
 */

public class HomeTitleBarView extends LinearLayout {
    private Context context;
    private View inflate;
    private TextView title;
    public ImageView iv_R;

    public HomeTitleBarView(Context context) {
        super(context);
    }

    public HomeTitleBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    private void init() {
        inflate = View.inflate(context, R.layout.home_titlebar_view, this);
        title = (TextView) inflate.findViewById(R.id.home_title);
        iv_R = (ImageView) inflate.findViewById(R.id.title_r);
    }

    public void setTitle(String titles) {
        title.setText(titles);
    }

    public void setROnclick(OnClickListener OnClickListener) {

        this.iv_R.setOnClickListener(OnClickListener);
    }

}
