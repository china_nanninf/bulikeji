package com.bulikeji.widget;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bulikeji.R;

import butterknife.Bind;


public class BottomShareDialog extends BottomSheetDialog implements View.OnClickListener {
    LinearLayout mLlShareFriend;
    LinearLayout mLlShareWx;
    LinearLayout mLlShareWeibo;
    LinearLayout mLlShareQq;
    LinearLayout mLlShareQqzone;


    @Bind(R.id.tv_share_cancel)
    TextView mTvShareCancel;

    private View rootview;

    private boolean showCircleShare;

    public void setOnShareListenter(OnShareListenter onShareListenter) {
        mOnShareListenter = onShareListenter;
    }

    private OnShareListenter mOnShareListenter;

    public BottomShareDialog(@NonNull Context context) {
        super(context);
        showCircleShare = false;
        initDialog(context);
    }

    public BottomShareDialog(@NonNull Context context, boolean showCircleShare) {
        super(context);
        this.showCircleShare = showCircleShare;
        initDialog(context);
    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        //        this.getWindow().addFlags(WindowManagerFlags.TranslucentStatus);
        //        this.getWindow().addFlags();
    }

    private void initDialog(Context context) {
        rootview = View.inflate(context, R.layout.view_ui_share, null);
        setContentView(rootview);
        mLlShareFriend = (LinearLayout) rootview.findViewById(R.id.ll_share_friend);
        mLlShareWx = (LinearLayout) rootview.findViewById(R.id.ll_share_wx);
        mLlShareWeibo = (LinearLayout) rootview.findViewById(R.id.ll_share_weibo);
        mLlShareQq = (LinearLayout) rootview.findViewById(R.id.ll_share_qq);
        mLlShareQqzone = (LinearLayout) rootview.findViewById(R.id.ll_share_qqzone);


        mTvShareCancel = (TextView) rootview.findViewById(R.id.tv_share_cancel);


        mLlShareFriend.setOnClickListener(this);
        mLlShareWx.setOnClickListener(this);
        mLlShareWeibo.setOnClickListener(this);
        mLlShareQq.setOnClickListener(this);
        mLlShareQqzone.setOnClickListener(this);
        mTvShareCancel.setOnClickListener(this);

    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_share_friend:
                if (mOnShareListenter == null) return;
                dismiss();
                mOnShareListenter.shareWX_frrend();
                break;
            case R.id.ll_share_wx:
                if (mOnShareListenter == null) return;
                dismiss();
                mOnShareListenter.shareWX();
                break;
            case R.id.ll_share_weibo:
                if (mOnShareListenter == null) return;
                dismiss();
                mOnShareListenter.shareWeiBo();
                break;
            case R.id.ll_share_qq:
                if (mOnShareListenter == null) return;
                dismiss();
                mOnShareListenter.shareQQ();
                break;
            case R.id.ll_share_qqzone:
                if (mOnShareListenter == null) return;
                dismiss();
                mOnShareListenter.shareQQZone();
                break;

            case R.id.tv_share_cancel:
                dismiss();
                break;
        }
    }

    public interface OnShareListenter {
        public void shareQQ();

        public void shareWX_frrend();

        public void shareWeiBo();

        public void shareQQZone();

        public void shareWX();

    }
}
