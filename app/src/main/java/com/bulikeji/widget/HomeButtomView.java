package com.bulikeji.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bulikeji.R;

/**
 * Created by Administrator on 2017/3/18 0018.
 */

public class HomeButtomView extends LinearLayout implements View.OnClickListener {

    private Context mContext;
    private View mRootView;
    private RelativeLayout home;
    private TextView tvHome;
    private ImageView ivHome;
    private RelativeLayout room;
    private TextView tvRoom;
    private ImageView ivRoom;
    private RelativeLayout chat;
    private TextView tvChat;
    private ImageView ivChat;
    public TextView roomMsgNumber;
    public TextView chatMsgNumber;

    public HomeButtomView(Context context) {
        super(context);
    }

    public HomeButtomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initView();
    }


    private void initView() {
        mRootView = View.inflate(mContext, R.layout.home_button_view, this);
        home = (RelativeLayout) mRootView.findViewById(R.id.home);
        tvHome = (TextView) mRootView.findViewById(R.id.tv_home);
        ivHome = (ImageView) mRootView.findViewById(R.id.iv_home);
        room = (RelativeLayout) mRootView.findViewById(R.id.room);
        tvRoom = (TextView) mRootView.findViewById(R.id.tv_room);
        ivRoom = (ImageView) mRootView.findViewById(R.id.iv_room);
        chat = (RelativeLayout) mRootView.findViewById(R.id.chat);
        tvChat = (TextView) mRootView.findViewById(R.id.tv_chat);
        ivChat = (ImageView) mRootView.findViewById(R.id.iv_chat);
        roomMsgNumber = (TextView) findViewById(R.id.room_msg_number);
        chatMsgNumber = (TextView) findViewById(R.id.chat_msg_number);
        home.setOnClickListener(this);
        room.setOnClickListener(this);
        chat.setOnClickListener(this);
        initStyle(0);

    }

    private void initStyle(int i) {
        tvHome.setTextColor(i == 0 ? mContext.getResources().getColor(R.color.title_color_checked) : mContext.getResources().getColor(R.color.title_color));
        tvRoom.setTextColor(i == 1 ? mContext.getResources().getColor(R.color.title_color_checked) : mContext.getResources().getColor(R.color.title_color));
        tvChat.setTextColor(i == 2 ? mContext.getResources().getColor(R.color.title_color_checked) : mContext.getResources().getColor(R.color.title_color));
        ivHome.setSelected(i == 0 ? true : false);
        ivRoom.setSelected(i == 1 ? true : false);
        ivChat.setSelected(i == 2 ? true : false);
    }


    @Override
    public void onClick(View v) {
        if (onButtomClickListener == null) return;
        switch (v.getId()) {
            case R.id.home:
                onButtomClickListener.setonButtomClickListener(0);
                initStyle(0);
                break;
            case R.id.room:
                onButtomClickListener.setonButtomClickListener(1);
                initStyle(1);
                break;
            case R.id.chat:
                onButtomClickListener.setonButtomClickListener(2);
                initStyle(2);
                break;
        }
    }

    public onButtomClickListener onButtomClickListener;

    public void setOnButtomClickListener(HomeButtomView.onButtomClickListener onButtomClickListener) {
        this.onButtomClickListener = onButtomClickListener;
    }

    public interface onButtomClickListener {
        void setonButtomClickListener(int index);
    }
}
