package com.bulikeji.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.bulikeji.base.BaseActivity;

/**
 * Created by Administrator on 2017/3/18 0018.
 */

public class ReceiverEvent extends BroadcastReceiver {
    public   static String CACEL_OKHTTP_ALL="cacel_okhttp_all";
    public   static String BACK="back";

    private final BaseActivity baseActivity;

    public ReceiverEvent(BaseActivity BaseActivity){
        baseActivity = BaseActivity;

    }
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("BL")) baseActivity.onEvent(intent);

    }
}
