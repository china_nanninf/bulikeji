package com.bulikeji.receiver;

import android.app.Activity;
import android.content.Intent;

import com.bulikeji.base.BaseActivity;

/**
 * Created by Administrator on 2017/3/18 0018.
 */

public class ReceiverUtil {

    public static void SendEvent(Activity BaseActivity, String data) {
        Intent intent = new Intent("BL");
        intent.putExtra("Code", data);
        BaseActivity.sendBroadcast(intent);

    }

    public static void SendEvent(BaseActivity BaseActivity,String action, String data) {
        Intent intent = new Intent("BL");
        intent.putExtra("Code", action);
        intent.putExtra("data", data);
        BaseActivity.sendBroadcast(intent);

    }

    public interface Contans {
        public static String UPDATE_USERINF = "UPDATE_USERINFO";//主页更新用户信息
        public static String SELECT_PHOTO = "SELECT_PHOTO";//选择相片
    }
}
