package com.bulikeji;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import com.bulikeji.acp.Acp;
import com.bulikeji.acp.AcpOptions;
import com.bulikeji.base.BaseActivity;
import com.bulikeji.base.BaseFragment;
import com.bulikeji.com.lzy.okhttputils.OkHttpUtils;
import com.bulikeji.com.lzy.okhttputils.utils.log.KLog;
import com.bulikeji.net.HttpManager;
import com.bulikeji.net.callback.BaseCallback;
import com.bulikeji.receiver.ReceiverUtil;
import com.bulikeji.ui.entity.UserInfoEntity;
import com.bulikeji.ui.fragment.ChatFragment;
import com.bulikeji.ui.fragment.HomeFragment;
import com.bulikeji.ui.fragment.RoomFragment;
import com.bulikeji.ui.login.LoginActivity;
import com.bulikeji.ui.login.RegisterActivity;
import com.bulikeji.utils.CheckPermissionsUtils;
import com.bulikeji.utils.PhoneInfo;
import com.bulikeji.utils.ToastUtil;
import com.bulikeji.utils.UserInfoSharedPreUtils;
import com.bulikeji.widget.HomeButtomView;
import com.hyphenate.EMContactListener;
import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMCmdMessageBody;
import com.hyphenate.chat.EMConversation;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chatui.Constant;
import com.hyphenate.chatui.DemoHelper;
import com.hyphenate.chatui.db.InviteMessgeDao;
import com.hyphenate.chatui.db.UserDao;
import com.hyphenate.chatui.runtimepermissions.PermissionsManager;
import com.hyphenate.chatui.runtimepermissions.PermissionsResultAction;
import com.hyphenate.chatui.ui.ChatActivity;
import com.hyphenate.chatui.ui.GroupsActivity;
import com.hyphenate.easeui.utils.EaseCommonUtils;
import com.hyphenate.util.EMLog;

import java.util.ArrayList;
import java.util.List;


import static com.darsh.multipleimageselect.helpers.Constants.REQUEST_CODE;

public class MainActivity extends BaseActivity implements HomeButtomView.onButtomClickListener {

    private com.bulikeji.widget.HomeButtomView buttomView;
    private ArrayList<Fragment> bodyList = new ArrayList<>();
    private HomeFragment homeFragment;
    private RoomFragment contactListFragment;
    private ChatFragment conversationListFragment;
    private long lastTime;
    private int current;
    private InviteMessgeDao inviteMessgeDao;
    public boolean isConflict = false;
    // user account was removed
    private boolean isCurrentAccountRemoved = false;


    @Override
    protected void initData() {

        homeFragment = new HomeFragment();
        contactListFragment = new RoomFragment();
        conversationListFragment = new ChatFragment();
        bodyList.add(homeFragment);
        bodyList.add(contactListFragment);
        bodyList.add(conversationListFragment);
        initBody(0);
    }

    @Override
    protected void setListener() {
        buttomView.setOnButtomClickListener(this);
    }

    @Override
    protected void initTitle() {
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!UserInfoSharedPreUtils.getInstance().isLogin(this)) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            return;
        }

        if (!UserInfoSharedPreUtils.getInstance().IsInputRegisterInfo(this)) {
            RegisterActivity.start(this);
            finish();
            return;
        }
        if (!isConflict && !isCurrentAccountRemoved) {
            updateUnreadLabel();
            updateUnreadAddressLable();
        }

        // unregister this event listener when this activity enters the
        // background
        DemoHelper sdkHelper = DemoHelper.getInstance();
        sdkHelper.pushActivity(this);

        EMClient.getInstance().chatManager().addMessageListener(messageListener);
    }

    @Override
    protected void onStop() {
        EMClient.getInstance().chatManager().removeMessageListener(messageListener);
        DemoHelper sdkHelper = DemoHelper.getInstance();
        sdkHelper.popActivity(this);

        super.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("isConflict", isConflict);
        outState.putBoolean(Constant.ACCOUNT_REMOVED, isCurrentAccountRemoved);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void initView() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String packageName = getPackageName();
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                Intent intent = new Intent();
                intent.setAction(android.provider.Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + packageName));
                startActivity(intent);
            }
        }
        requestPermissions();
        buttomView = (HomeButtomView) findViewById(R.id.buttom_view);
        showExceptionDialogFromIntent(getIntent());

        inviteMessgeDao = new InviteMessgeDao(this);
        UserDao userDao = new UserDao(this);
        registerBroadcastReceiver();


        EMClient.getInstance().contactManager().setContactListener(new MyContactListener());
        //debug purpose only

    }

    private void initBody(int current) {
        this.current = current;
        Fragment BaseFragment = bodyList.get(current);
        getSupportFragmentManager().beginTransaction().replace(R.id.body, BaseFragment).commit();
    }


    @Override
    protected boolean isApplyReceiver() {
        return true;
    }

    @Override
    public void onEvent(Intent intent) {
        super.onEvent(intent);
        switch (code) {
            case ReceiverUtil.Contans.UPDATE_USERINF:
                if (homeFragment != null) {
                    homeFragment.initUserInfo();
                }
                break;
        }
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }


    @Override
    public void setonButtomClickListener(int index) {
        switch (index) {
            case 0:
                initBody(0);
                break;
            case 1:
                initBody(1);
                break;
            case 2:
                initBody(2);
                break;
        }
    }

    public static void start(Context Context, UserInfoEntity base) {
        Intent Intent = new Intent(Context, MainActivity.class);
        UserInfoSharedPreUtils.getInstance().put(Context, base);
        Context.startActivity(Intent);
    }

    public static void start(Context Context) {
        Intent Intent = new Intent(Context, MainActivity.class);
        Context.startActivity(Intent);
    }

    public boolean ispermitfinish = true;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            KLog.d("onKeyDown", ispermitfinish);
            if (!ispermitfinish) return true;
            if (System.currentTimeMillis() - lastTime < 2000) {

                return super.onKeyDown(keyCode, event);
            } else {
                lastTime = System.currentTimeMillis();
                ToastUtil.getInstance().show("再按一次退出应用");
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    EMMessageListener messageListener = new EMMessageListener() {

        @Override
        public void onMessageReceived(List<EMMessage> messages) {
            // notify new message
            for (EMMessage message : messages) {
                DemoHelper.getInstance().getNotifier().onNewMsg(message);
            }
            refreshUIWithMessage();
        }

        @Override
        public void onCmdMessageReceived(List<EMMessage> messages) {

            //end of red packet code
            refreshUIWithMessage();
        }

        @Override
        public void onMessageRead(List<EMMessage> messages) {
        }

        @Override
        public void onMessageDelivered(List<EMMessage> message) {
        }

        @Override
        public void onMessageChanged(EMMessage message, Object change) {
        }
    };

    private void refreshUIWithMessage() {
        runOnUiThread(new Runnable() {
            public void run() {
                // refresh unread count
                updateUnreadLabel();
                if (current == 2) {
                    // refresh conversation list
                    if (conversationListFragment != null) {
                        conversationListFragment.refresh();
                    }
                }
            }
        });
    }

    /**
     * update unread message count
     */
    public void updateUnreadLabel() {
        int count = getUnreadMsgCountTotal();
        if (count > 0) {
            buttomView.chatMsgNumber.setText(String.valueOf(count));
            buttomView.chatMsgNumber.setVisibility(View.VISIBLE);
        } else {
            buttomView.chatMsgNumber.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * update the total unread count
     */
    public void updateUnreadAddressLable() {
        runOnUiThread(new Runnable() {
            public void run() {
                int count = getUnreadAddressCountTotal();
                if (count > 0) {
                    buttomView.roomMsgNumber.setVisibility(View.VISIBLE);
                } else {
                    buttomView.roomMsgNumber.setVisibility(View.INVISIBLE);
                }
            }
        });

    }

    /**
     * get unread event notification count, including application, accepted, etc
     *
     * @return
     */
    public int getUnreadAddressCountTotal() {
        int unreadAddressCountTotal = 0;
        unreadAddressCountTotal = inviteMessgeDao.getUnreadMessagesCount();
        return unreadAddressCountTotal;
    }

    /**
     * get unread message count
     *
     * @return
     */
    public int getUnreadMsgCountTotal() {
        int unreadMsgCountTotal = 0;
        int chatroomUnreadMsgCount = 0;
        unreadMsgCountTotal = EMClient.getInstance().chatManager().getUnreadMessageCount();
        for (EMConversation conversation : EMClient.getInstance().chatManager().getAllConversations().values()) {
            if (conversation.getType() == EMConversation.EMConversationType.ChatRoom)
                chatroomUnreadMsgCount = chatroomUnreadMsgCount + conversation.getUnreadMsgCount();
        }
        return unreadMsgCountTotal - chatroomUnreadMsgCount;
    }

    private void showExceptionDialogFromIntent(Intent intent) {
        KLog.e("showExceptionDialogFromIntent");
        if (!isExceptionDialogShow && intent.getBooleanExtra(Constant.ACCOUNT_CONFLICT, false)) {
            showExceptionDialog(Constant.ACCOUNT_CONFLICT);
        } else if (!isExceptionDialogShow && intent.getBooleanExtra(Constant.ACCOUNT_REMOVED, false)) {
            showExceptionDialog(Constant.ACCOUNT_REMOVED);
        } else if (!isExceptionDialogShow && intent.getBooleanExtra(Constant.ACCOUNT_FORBIDDEN, false)) {
            showExceptionDialog(Constant.ACCOUNT_FORBIDDEN);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        showExceptionDialogFromIntent(intent);
    }

    private void showExceptionDialog(String exceptionType) {
        isExceptionDialogShow = true;
        DemoHelper.getInstance().logout(false, null);
        String st = getResources().getString(R.string.Logoff_notification);
        if (!MainActivity.this.isFinishing()) {
            // clear up global variables
            try {
                if (exceptionBuilder == null)
                    exceptionBuilder = new android.app.AlertDialog.Builder(MainActivity.this);
                exceptionBuilder.setTitle(st);
                exceptionBuilder.setMessage(getExceptionMessageId(exceptionType));
                exceptionBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        exceptionBuilder = null;
                        isExceptionDialogShow = false;
                        finish();
                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
                exceptionBuilder.setCancelable(false);
                exceptionBuilder.create().show();
                isConflict = true;
            } catch (Exception e) {
                KLog.e("---------color conflictBuilder error" + e.getMessage());
            }
        }
    }

    private android.app.AlertDialog.Builder exceptionBuilder;
    private boolean isExceptionDialogShow = false;
    private BroadcastReceiver internalDebugReceiver;
    private BroadcastReceiver broadcastReceiver;
    private LocalBroadcastManager broadcastManager;

    public class MyContactListener implements EMContactListener {
        @Override
        public void onContactAdded(String username) {}
        @Override
        public void onContactDeleted(final String username) {
            runOnUiThread(new Runnable() {
                public void run() {
                    if (ChatActivity.activityInstance != null && ChatActivity.activityInstance.toChatUsername != null &&
                            username.equals(ChatActivity.activityInstance.toChatUsername)) {
                        String st10 = getResources().getString(R.string.have_you_removed);
                        Toast.makeText(MainActivity.this, ChatActivity.activityInstance.getToChatUsername() + st10, Toast.LENGTH_LONG)
                                .show();
                        ChatActivity.activityInstance.finish();
                    }
                }
            });
            updateUnreadAddressLable();
        }
        @Override
        public void onContactInvited(String username, String reason) {}
        @Override
        public void onFriendRequestAccepted(String username) {}
        @Override
        public void onFriendRequestDeclined(String username) {}
    }

    private int getExceptionMessageId(String exceptionType) {
        if (exceptionType.equals(Constant.ACCOUNT_CONFLICT)) {
            return R.string.connect_conflict;
        } else if (exceptionType.equals(Constant.ACCOUNT_REMOVED)) {
            return R.string.em_user_remove;
        } else if (exceptionType.equals(Constant.ACCOUNT_FORBIDDEN)) {
            return R.string.user_forbidden;
        }
        return R.string.Network_error;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (exceptionBuilder != null) {
            exceptionBuilder.create().dismiss();
            exceptionBuilder = null;
            isExceptionDialogShow = false;
        }
        unregisterBroadcastReceiver();

        try {
            unregisterReceiver(internalDebugReceiver);
        } catch (Exception e) {
        }

    }
    @TargetApi(23)
    private void requestPermissions() {
        PermissionsManager.getInstance().requestAllManifestPermissionsIfNecessary(this, new PermissionsResultAction() {
            @Override
            public void onGranted() {
            }

            @Override
            public void onDenied(String permission) {
            }
        });
    }
    private void registerBroadcastReceiver() {
        broadcastManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constant.ACTION_CONTACT_CHANAGED);
        intentFilter.addAction(Constant.ACTION_GROUP_CHANAGED);
        broadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                updateUnreadLabel();
                updateUnreadAddressLable();
                if (current == 2) {
                    // refresh conversation list
                    if (conversationListFragment != null) {
                        conversationListFragment.refresh();
                    }
                } else if (current == 1) {
                    if(contactListFragment != null) {
                        contactListFragment.refresh();
                    }
                }
                String action = intent.getAction();
                if(action.equals(Constant.ACTION_GROUP_CHANAGED)){
                    if (EaseCommonUtils.getTopActivity(MainActivity.this).equals(GroupsActivity.class.getName())) {
                        GroupsActivity.instance.onResume();
                    }
                }

            }
        };
        broadcastManager.registerReceiver(broadcastReceiver, intentFilter);
    }
    private void unregisterBroadcastReceiver() {
        broadcastManager.unregisterReceiver(broadcastReceiver);
    }
}
