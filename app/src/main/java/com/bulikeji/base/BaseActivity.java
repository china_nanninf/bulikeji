package com.bulikeji.base;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.View;

import com.bulikeji.acp.AcpListener;
import com.bulikeji.com.lzy.okhttputils.OkHttpUtils;
import com.bulikeji.com.lzy.okhttputils.utils.log.KLog;
import com.bulikeji.receiver.ReceiverEvent;
import com.bulikeji.utils.SharedPreferenceUtils;
import com.bulikeji.utils.StatusBarTool;
import com.bulikeji.utils.ToastUtil;
import com.bulikeji.utils.UserInfoSharedPreUtils;

import java.util.List;


/**
 * Activity基类
 */
public abstract class BaseActivity extends FragmentActivity implements AcpListener, View.OnClickListener {


    public Context baseContext;
    private boolean applyReceiver;
    private ReceiverEvent receiverUtils;
    protected String code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        StatusBarTool.color(this);
        setContentView(getContentView());
        receiverUtils = new ReceiverEvent(this);
        registerReceiver(receiverUtils, new IntentFilter("BL"));
        baseContext = this;

        if (getContentView() > 0) {
            initView();
            initData();
            initTitle();

            setListener();
            applyReceiver = isApplyReceiver();
        }

    }

    @Override
    public void onGranted() {

    }

    @Override
    public void onDenied(List<String> permissions) {

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        newConfig.setToDefaults();
    }

    @Override
    public Resources getResources() {
        Resources res = super.getResources();
        Configuration config = new Configuration();
        config.setToDefaults();
        res.updateConfiguration(config, res.getDisplayMetrics());

        return res;
    }

    /**
     * 初始化数据
     */
    protected abstract void initData();

    protected abstract void setListener();

    /**
     * 初始化View
     */
    protected abstract void initTitle();

    protected abstract void initView();

    protected abstract boolean isApplyReceiver();


    protected abstract int getContentView();

    @Override
    protected void onDestroy() {
        //取消网络请求
        OkHttpUtils.getInstance().cancelTag(this);
        if (receiverUtils != null) {
            unregisterReceiver(receiverUtils);
        }
        super.onDestroy();
    }

    //是否接收事件
    public void onEvent(Intent intent) {
        if (!applyReceiver) return;
        code = intent.getStringExtra("Code");
         /*
     //取消全部网络请求
                  */
        if (intent.getStringExtra("Code").equals(ReceiverEvent.CACEL_OKHTTP_ALL)) {
            OkHttpUtils.getInstance().cancelAll();
        }
        if (intent.getStringExtra("Code").equals(ReceiverEvent.BACK)) {
            finish();
        }
    }

    ;

    @Override
    public void onClick(View v) {

    }


}
