package com.bulikeji.base;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bulikeji.com.lzy.okhttputils.OkHttpUtils;

/**
 * Created by Administrator on 2017/3/18 0018.
 */

public abstract class BaseFragment extends Fragment implements View.OnClickListener {

    protected View mRootView;
    private FragmentActivity mActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = View.inflate(getActivity(),getContentView(),null);

        mActivity = getActivity();

        if (getContentView() > 0) {
            initView();
            initData();
            initTitle();

            setListener();
        }
        return mRootView ;
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        newConfig.setToDefaults();
    }



    /**
     * 初始化数据
     */
    protected abstract void initData();

    protected abstract void setListener();

    /**
     * 初始化View
     */
    protected abstract void initTitle();

    protected abstract void initView();


    @Override
    public void onClick(View v) {

    }

    protected abstract int getContentView();

    @Override
    public void onDestroy() {
        super.onDestroy();
        //取消网络请求
        OkHttpUtils.getInstance().cancelTag(this);

    }


}

