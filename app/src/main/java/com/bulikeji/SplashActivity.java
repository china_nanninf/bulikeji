package com.bulikeji;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;

import com.bulikeji.net.HttpManager;
import com.bulikeji.net.callback.BaseCallback;
import com.bulikeji.ui.entity.UserInfoEntity;
import com.bulikeji.ui.login.LoginActivity;
import com.bulikeji.utils.AppInfo;
import com.bulikeji.utils.CheckPermissionsUtils;
import com.bulikeji.utils.PhoneInfo;
import com.bulikeji.utils.ToastUtil;
import com.bulikeji.utils.UserInfoSharedPreUtils;
import com.tencent.connect.UserInfo;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //是6.0    没有授权
        if (CheckPermissionsUtils.isSDK_M()) {
            if (!CheckPermissionsUtils.CheckPermissions(SplashActivity.this, Manifest.permission.READ_PHONE_STATE)) {
                CheckPermissionsUtils.requestPsi(SplashActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);

            }
            if (CheckPermissionsUtils.CheckPermissions(SplashActivity.this, Manifest.permission.READ_PHONE_STATE))
                getIemi();

        }
        TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceid = tm.getDeviceId();
        String tel = tm.getLine1Number();//手机号码
        String imei = tm.getSimSerialNumber();
        String imsi = tm.getSubscriberId();

        setContentView(R.layout.activity_splash);
        Handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (UserInfoSharedPreUtils.getInstance().isLogin(SplashActivity.this)) {
                    MainActivity.start(SplashActivity.this);
                    finish();
//                    TokenLogin();
                    return;
                }

                LoginActivity.start(SplashActivity.this);
                finish();
            }
        }, 2000);

    }


    Handler Handler = new Handler();

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) return true;
        return super.onKeyDown(keyCode, event);
    }

    private void TokenLogin() {

        UserInfoEntity user = new UserInfoEntity();
        user.setData(new UserInfoEntity.DataBean());

        user.getData().setPhone((String) UserInfoSharedPreUtils.getInstance().getPhone(this));
        user.getData().setToken((String) UserInfoSharedPreUtils.getInstance().getToken(this));
        user.getData().setDeviceNo(imsi);
        HttpManager.tokenLogin(user, this, new BaseCallback<UserInfoEntity>() {
            @Override
            public void onLogicSuccess(UserInfoEntity userInfoEntity) {
                super.onLogicSuccess(userInfoEntity);
                if (userInfoEntity != null && userInfoEntity.getData() != null) {
                    UserInfoSharedPreUtils.getInstance().put(SplashActivity.this, userInfoEntity);
                    MainActivity.start(SplashActivity.this);
                    finish();
                    return;
                }
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                finish();

            }

            @Override
            public void onLogicFailure(UserInfoEntity userInfoEntity) {
                super.onLogicFailure(userInfoEntity);
                ToastUtil.getInstance().show(userInfoEntity.ret_msg);
                LoginActivity.start(SplashActivity.this);
                finish();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            int checkCallPhonePermission =
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
            if (checkCallPhonePermission != PackageManager.PERMISSION_GRANTED) {
                ToastUtil.getInstance().show("请在手机管家授予唯一标识权限");
                return;
            }
            getIemi();


        }

    }

    private String imsi = "";

    public void getIemi() {

        imsi = AppInfo.getModelId(this);
    }
}
