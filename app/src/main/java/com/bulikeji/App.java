package com.bulikeji;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.support.multidex.MultiDex;

import com.bulikeji.com.lzy.okhttputils.OkHttpUtils;
import com.bulikeji.com.lzy.okhttputils.cache.CacheMode;
import com.bulikeji.com.lzy.okhttputils.model.HttpParams;
import com.bulikeji.com.lzy.okhttputils.utils.log.KLog;

import com.hyphenate.chatui.DemoHelper;
import com.orm.SugarContext;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.beta.Beta;
import com.tencent.bugly.crashreport.CrashReport;
import com.tencent.mm.sdk.openapi.IWXAPI;

import static com.tencent.bugly.beta.tinker.TinkerManager.getApplication;

/**
 * Created by Administrator on 2017/3/17 0017.
 */
public class App extends Application {

    private static Context applicationContext;
    public static IWXAPI sApi;
    public static String currentUserNick = "";

    private HttpParams params;
    private App instance;

    //ssad
    @Override
    public void onCreate() {
        super.onCreate();
        applicationContext = getApplicationContext();
        KLog.init(BuildConfig.DEBUG);
        inithttp();
        SugarContext.init(this);

//        sApi = WXAPIFactory.createWXAPI(this, "wx4868b35061f87885");
        CrashReport.UserStrategy UserStrategy = new CrashReport.UserStrategy(getApplicationContext());
        UserStrategy.setAppChannel("Android");  //设置渠道
        UserStrategy.setAppVersion("3.0.0");      //App的版本
        UserStrategy.setAppPackageName("com.bulikeji");  //App的包名
        UserStrategy.setAppReportDelay(20000);
           /* Bugly SDK初始化
        * 参数1：上下文对象
        * 参数2：APPID，平台注册时得到,注意替换成你的appId
        * 参数3：是否开启调试模式，调试模式下会输出'CrashReport'tag的日志   发布的时候记得修改为  false
        */
        Bugly.init(getApplicationContext(), "9c9f47753e", true,UserStrategy);
        instance = this;

        //init demo helper
        DemoHelper.getInstance().init(applicationContext);



    }

    public static Context getInstance() {
        return applicationContext;
    }


    private void inithttp() {
        params = new HttpParams();
        //必须调用初始化
        OkHttpUtils.init(this);
        //以下都不是必须的，根据需要自行选择
        OkHttpUtils.getInstance()//
                .debug("OkHttpUtils")                                              //是否打开调试
                .setConnectTimeout(OkHttpUtils.DEFAULT_MILLISECONDS)               //全局的连接超时时间
                .setReadTimeOut(OkHttpUtils.DEFAULT_MILLISECONDS)                  //全局的读取超时时间
                .setWriteTimeOut(OkHttpUtils.DEFAULT_MILLISECONDS)
                .setCacheMode(CacheMode.NO_CACHE)
                .addCommonParams(params);
    }
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        // you must install multiDex whatever tinker is installed!
        MultiDex.install(base);
        // 安装tinker
        Beta.installTinker();
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public void registerActivityLifecycleCallback(Application.ActivityLifecycleCallbacks callbacks) {
        getApplication().registerActivityLifecycleCallbacks(callbacks);
    }

}
