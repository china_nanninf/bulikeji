package com.bulikeji.net;

import com.bulikeji.utils.Explain;

public class APIURL {

    public static final boolean DEBUG = false;
    private static String HOST = DEBUG ? "http://119.23.10.65:8080/live-android" : "http://live.bulikeji.com";


    @Explain("2.1.5手机号登录")
    public static String PHONE_LOGIN = HOST + "/live/users/login/phone";
    @Explain("验证码")
    public static String GET_CODE = HOST + "/live/users/code/get";
    @Explain("检验验证码")
    public static String CHECK_CODE = HOST + "/live/users/code/check";
    @Explain("注册")
    public static String REGISTER = HOST + "/live/users/register";
    @Explain("更新信息")
    public static String UPDATE = HOST + "/live/users/update";
    @Explain("Token登录")
    public static String TOKEN_LOGIN = HOST + "/live/users/login/token";
    @Explain("2.1.7上传头像（/live/users/thumb）")
    public static String UPLOAD_USER_ICON = HOST + "/live/users/thumb";
    @Explain("2.1.8重置密码（/live/users/password/reset）")
    public static String REST_PASSWORD = HOST + "/live/users/password/reset";
}