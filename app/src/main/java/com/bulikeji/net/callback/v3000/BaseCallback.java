package com.bulikeji.net.callback.v3000;

import android.content.Context;
import android.support.annotation.Nullable;

import com.bulikeji.App;
import com.bulikeji.com.lzy.okhttputils.callback.AbsCallback;
import com.bulikeji.com.lzy.okhttputils.request.BaseRequest;
import com.bulikeji.com.lzy.okhttputils.utils.log.KLog;
import com.bulikeji.ui.entity.BaseResponse;
import com.bulikeji.ui.entity.Global;
import com.bulikeji.utils.DateUtils;
import com.bulikeji.utils.GsonConvert;
import com.bulikeji.utils.ToastUtil;
import com.google.gson.stream.JsonReader;


import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Request;
import okhttp3.Response;


public class BaseCallback<T extends BaseResponse> extends AbsCallback<T> {

    BaseRequest request;
    Response response;
    String respString = "";
    private final static String TAG = "OkHttpUtils";

    @Override
    public void onBefore(BaseRequest request) {
        super.onBefore(request);
        this.request = request;
        Locale locale = Locale.getDefault();
        String language = locale.getLanguage();
        request.params("language", "zh_cn");
        //        request.params("language", language);
        Context context = App.getInstance();
//        if (!UserInfoSharedPreUtils.getInstance().getSign(context).equals("")) {
//            request.params("sign", UserInfoSharedPreUtils.getInstance().getSign(context));
//        }
    }

    public BaseCallback() {
        super();
    }

    @Override
    public T parseNetworkResponse(Response response) throws Exception {
        String time = response.headers().get("Date");
        try {
        Global.CurrentServerTimeMillis = DateUtils.parseGMTToMillis(time);
            //          Global.CurrentServerTime = DateUtils.parseGMTToMillis(time);
            //          KLog.d("====000", DateUtils.parseGMTToMillis(time) + "");
            KLog.d("====000", DateUtils.parseGMTToMillis(time) + "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.response = response;
        JsonReader jsonReader = new JsonReader(response.body().charStream());
        Type genType = getClass().getGenericSuperclass();
        Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
        Type type = params[0];
        if (!(type instanceof ParameterizedType)) throw new IllegalStateException("没有填写泛型参数");
        Type rawType = ((ParameterizedType) type).getRawType();
        if (rawType == BaseResponse.class) {
            BaseResponse newsResponse = GsonConvert.fromJson(jsonReader, type);
            return (T) newsResponse;
        }
        throw new IllegalStateException("数据无法解析!");
    }

    @Override
    public void onError(boolean isFromCache, Call call, @Nullable Response response,
                        @Nullable Exception e) {
        super.onError(isFromCache, call, response, e);
        if (e == null) return;
        if (e.getMessage() == null) return;
        //        KLog.d("sendFailResultCallback", "onError");
        KLog.d("sendFailResultCallback", e.getMessage());
//    ToastUtil.getInstance().show(e.getMessage());
    }

    @Override
    public void onAfter(boolean isFromCache, @Nullable T t, Call call, @Nullable Response response,
                        @Nullable Exception e) {
        super.onAfter(isFromCache, t, call, response, e);
    }

    @Override
    public void onResponse(boolean isFromCache, T t, Request request, @Nullable Response response) {
        if (t.ret_code==200) {
            onLogicSuccess(t);
        } else {
            onLogicFailure(t);
        }
    }

    public void onLogicSuccess(T t) {
    }

    ;

    public void onLogicFailure(T t) {
        if (t.ret_msg != null) {
            ToastUtil.getInstance().show("" + t.ret_msg);
        }

    }
}
