package com.bulikeji.net.callback;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.bulikeji.com.lzy.okhttputils.callback.AbsCallback;


import java.text.ParseException;

import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by lujiang on 2016/12/26.
 * 网络字符串请求回调
 */

public class BaseStringCallback<T> extends AbsCallback<T> {
    @Override
    public T parseNetworkResponse(Response response) throws Exception {

        String responseData = response.body().string();

        if (TextUtils.isEmpty(responseData)) return null;
        return (T)responseData;
    }

    @Override
    public void onResponse(boolean isFromCache, T t, Request request, @Nullable Response response) throws ParseException {

    }
}
