package com.bulikeji.net.callback;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.bulikeji.App;
import com.bulikeji.com.lzy.okhttputils.callback.AbsCallback;
import com.bulikeji.com.lzy.okhttputils.request.BaseRequest;
import com.bulikeji.com.lzy.okhttputils.utils.log.KLog;
import com.bulikeji.net.APIURL;
import com.bulikeji.ui.entity.Base;
import com.bulikeji.utils.Explain;
import com.bulikeji.utils.NumberConstant;
import com.bulikeji.utils.ToastUtil;
import com.google.gson.Gson;


import java.lang.reflect.Field;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;


import static com.bulikeji.com.lzy.okhttputils.interceptor.LoggerInterceptor.isText;

public class BaseCallback<T extends Base> extends AbsCallback<T> {

    BaseRequest request;
    Response response;
    String respString = "";
    private final static String TAG = "OkHttpUtils";

    @Override
    public void onBefore(BaseRequest request) {
        super.onBefore(request);
        this.request = request;

        Context context = App.getInstance();

    }

    public BaseCallback() {
        super();
    }

    @Override
    public T parseNetworkResponse(Response response) throws Exception {

        this.response = response;
        String responseData = response.body().string();
        respString = responseData;

        KLog.e("parseNetworkResponse >>>> " + respString);

        if (TextUtils.isEmpty(responseData)) return null;
        if (type != null)
            return new Gson().fromJson(responseData, type);
        return null;
    }

    @Override
    public void onError(boolean isFromCache, Call call, @Nullable Response response, @Nullable Exception e) {
        super.onError(isFromCache, call, response, e);

        if (e == null)
            return;
        if (e.getMessage() == null)
            return;
        printErrorLogcat(response);
        if (APIURL.DEBUG) {
            ToastUtil.getInstance().show(e.getMessage());
        } else {
            ToastUtil.getInstance().show("网络错误，请稍后再试！");
        }
    }

    @Override
    public void onAfter(boolean isFromCache, @Nullable T t, Call call, @Nullable Response response, @Nullable Exception e) {
        super.onAfter(isFromCache, t, call, response, e);

    }

    @Override
    public void onResponse(boolean isFromCache, T t, Request request, @Nullable Response response) {

        if (t.ret_code == 200) {
            onLogicSuccess(t);
        } else {
            onLogicFailure(t);
        }
        printLogcat();
    }

    public void onLogicSuccess(T t) {

    }


    public void onLogicFailure(T t) {

        if (t.ret_msg != null) {


            Response.Builder builder = response.newBuilder();
            Response res = builder.build();

            KLog.e("=======LOGIN_CLIENT_IMEI_ERROR===========");
            Log.e(TAG, res.request().url() + "");
        }


    }

    /**
     * 打印网络请求错误时的日志
     *
     * @param response
     */
    private void printErrorLogcat(Response response) {
        Log.e(TAG, " ========================================================================== ");

        if (response == null) {
            Log.e(TAG, " 网络请求错误, response为NULL");
        } else {
            Response.Builder builder = response.newBuilder();
            Response res = builder.build();

            String[] params = request.getParams().toString().replaceAll("=", " = ").split("&");

            Log.e(TAG, " 请求错误 [ " + getExplain(res.request().url().toString()) + " ]");
            Log.e(TAG, " 接 口: " + res.request().url());
            Log.e(TAG, " 响应码: " + res.code());
            Log.e(TAG, " 参 数: ");
            for (int i = 0; i < params.length; i++) {
                String parContent = "";
                if (i < NumberConstant.PARAMS_SORT.length)
                    parContent += NumberConstant.PARAMS_SORT[i] + " ";
                else parContent += "..";
                parContent += (params[i] + "  ");
                Log.e(TAG, parContent);
            }
        }

        Log.e(TAG, " ========================================================================== ");

    }

    private static int responseCount = 1;

    /**
     * 打印日志
     */
    private void printLogcat() {

        if (!APIURL.DEBUG) return;
        new Thread(new Runnable() {
            @Override
            public void run() {
                String edge = "";
                try {

                    Response.Builder builder = response.newBuilder();
                    Response res = builder.build();

                    String urlAndParams = res.request().url() + "?" + request.getParams();

                    for (int i = 0; i < urlAndParams.length() + 5; i++) {
                        edge += "═";
                    }

                    String[] params = request.getParams().toString().replaceAll("=", " = ").split("&");

                    Log.i(TAG, "╔" + edge);
                    Log.i(TAG, "║ [ " + responseCount + "." + getExplain(res.request().url().toString()) + " ]");
                    Log.i(TAG, "║ 接 口: " + res.request().url());
                    Log.i(TAG, "║ 参 数: ");
                    for (int i = 0; i < params.length; i++) {
                        String parContent = "║  ";
                        if (i < NumberConstant.PARAMS_SORT.length)
                            parContent += NumberConstant.PARAMS_SORT[i] + " ";
                        else parContent += "..";
                        parContent += params[i];

                        Log.i(TAG, parContent);
                    }
                    Log.i(TAG, "║ " + "");
                    Log.i(TAG, "║ 地 址: " + urlAndParams);
                    Log.i(TAG, "║ " + "");
                    Log.i(TAG, "║ 响应码: " + res.code());

                    ResponseBody body = res.body();
                    if (body != null) {
                        MediaType mediaType = body.contentType();
                        if (mediaType != null) {
                            if (isText(mediaType)) {
                                Log.i(TAG, "║ 结 果: ");
                                KLog.json(TAG, respString);
                            } else {
                                Log.i(TAG, "║ 结 果: " + " >>>>>>>>>>> 非文本");
                            }
                        }
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    responseCount++;
                    Log.i(TAG, "╚" + edge);
                }
            }
        }).start();

    }

    private static Field[] fs = null;

    /**
     * 获取请求的含义
     *
     * @return
     */
    private String getExplain(String url) {
        Class<APIURL> apiUrlClass = APIURL.class;
        try {
            if (null == fs) {
                fs = apiUrlClass.getDeclaredFields();
            }
            for (Field f : fs) {
                f.setAccessible(true);
                Object val = f.get(apiUrlClass.newInstance());//得到属性值

                //过滤掉去三个属性
                if (url.contains(val.toString()) && !f.getName().contains("HOST")) {
                    Explain explain = f.getAnnotation(Explain.class);
                    return explain.value();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "不可描述 ≧▽≦";
    }
}
