package com.bulikeji.net.callback;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.bulikeji.com.lzy.okhttputils.callback.AbsCallback;

import okhttp3.Response;

public abstract class BitmapCallback extends AbsCallback<Bitmap> {

    @Override
    public Bitmap parseNetworkResponse(Response response) throws Exception {
        return BitmapFactory.decodeStream(response.body().byteStream());
    }
}
