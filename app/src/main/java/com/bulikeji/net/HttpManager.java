package com.bulikeji.net;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bulikeji.com.lzy.okhttputils.OkHttpUtils;
import com.bulikeji.com.lzy.okhttputils.callback.BitmapCallback;
import com.bulikeji.com.lzy.okhttputils.utils.log.KLog;
import com.bulikeji.net.callback.BaseCallback;
import com.bulikeji.ui.entity.UserInfoEntity;
import com.bulikeji.utils.PhotoUtil;
import com.bulikeji.utils.UserInfoSharedPreUtils;

import java.io.File;
import java.text.ParseException;

import okhttp3.Call;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Administrator on 2017/3/18 0018.
 */

public class HttpManager {
    //下载用户头像
    public static void DownloadPortrait(String url, @NonNull final ImageView imageView, final Activity activity) {
        OkHttpUtils.get(url).tag(activity).execute(new BitmapCallback() {
            @Override
            public void onResponse(boolean isFromCache, Bitmap bitmap, Request request,
                                   @Nullable Response response) throws ParseException {
                PhotoUtil.saveCutBitmapForCache(activity, bitmap);
                imageView.setImageBitmap(bitmap);
            }

            @Override
            public void onError(boolean isFromCache, Call call, @Nullable Response response,
                                @Nullable Exception e) {
                super.onError(isFromCache, call, response, e);
                PhotoUtil.getHeadPhotoFileRaw().delete();
                KLog.d("onError", "onError");
            }
        });
    }
    //登录
    public static void login(String phone, String psw, Context Context, String Iemi, BaseCallback BaseCallback) {

        OkHttpUtils.post(APIURL.PHONE_LOGIN).params("phone", phone)
                .params("password", psw).params("deviceNo", Iemi)
                .tag(Context).execute(BaseCallback);
    }

    //获取验证码

    public static void getCode(String phone, Context Context,String codeType ,BaseCallback BaseCallback) {

        OkHttpUtils.get(APIURL.GET_CODE).params("phone", phone).params("codeType", codeType)
                .tag(Context).execute(BaseCallback);
    }

    //检验验证码

    public static void checkCode(String phone, String code, Context Context, BaseCallback BaseCallback) {

        OkHttpUtils.post(APIURL.CHECK_CODE).params("phone", phone)
                .params("code", code).tag(Context).execute(BaseCallback);
    }

    //注册

    public static void register(String phone, String code, String iemi, String psw, Context Context, BaseCallback BaseCallback) {

        OkHttpUtils.post(APIURL.REGISTER).params("phone", phone)
                .params("code", code).params("password", psw)
                .params("deviceNo", iemi).tag(Context).execute(BaseCallback);
    }

    //更新信息

    public static void update(UserInfoEntity UserInfoEntity, Context Context, BaseCallback BaseCallback) {

        com.bulikeji.ui.entity.UserInfoEntity.DataBean data = UserInfoEntity.getData();
        OkHttpUtils.post(APIURL.UPDATE)
                .params("token", data.getToken())
                .params("phone", data.getPhone())
                .params("name", data.getName())
                .params("sex", data.getSex())
                .params("age", data.getAge() + "")
                .params("school", data.getSchool())
                .params("thumb", data.getThumb())
                .params("signature", (String) data.getSignature())
                .params("deviceNo", data.getDeviceNo()).tag(Context).execute(BaseCallback);
    }

    //Token登录

    public static void tokenLogin(UserInfoEntity UserInfoEntity, Context Context, BaseCallback BaseCallback) {

        OkHttpUtils.post(APIURL.TOKEN_LOGIN)
                .params("phone", UserInfoEntity.getData().getPhone())
                .params("token", UserInfoEntity.getData().getToken())
                .params("deviceNo", UserInfoEntity.getData().getDeviceNo())
                .tag(Context).execute(BaseCallback);
    }

    //7上传头像


    public static void uploadIcon(File file, String iemi, Context Context, BaseCallback BaseCallback) {

        OkHttpUtils.post(APIURL.UPLOAD_USER_ICON)
                .params("token", UserInfoSharedPreUtils.getInstance().getToken(Context) + "")
                .params("phone", UserInfoSharedPreUtils.getInstance().getPhone(Context) + "")
                .params("deviceNo",iemi + "")
                .params("file", file)
                .tag(Context).execute(BaseCallback);
    }
    //8重置密码


    public static void resSetPsw(UserInfoEntity UserInfoEntity, String code, Context Context, BaseCallback BaseCallback) {

        OkHttpUtils.post(APIURL.REST_PASSWORD)
                .params("phone", UserInfoEntity.getData().getPhone())
                .params("password", UserInfoEntity.getData().getPassword())
                .params("deviceNo", UserInfoEntity.getData().getDeviceNo())
                .params("code", code)
                .tag(Context).execute(BaseCallback);
    }
}
