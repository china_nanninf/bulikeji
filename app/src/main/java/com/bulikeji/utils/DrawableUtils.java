package com.bulikeji.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bulikeji.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.stream.BaseGlideUrlLoader;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.signature.StringSignature;


import static com.bumptech.glide.request.target.Target.SIZE_ORIGINAL;

public class DrawableUtils {

    private static final String CHATTING_DIR = "/chatting_cache";

    public interface MyDataModel {
        public String buildUrl(int width, int height);
    }

    class MyUrlLoader extends BaseGlideUrlLoader<MyDataModel> {
        public MyUrlLoader(Context context) {
            super(context);
        }

        @Override
        protected String getUrl(MyDataModel model, int width, int height) {
            return model.buildUrl(width, height);
        }
    }

    public static void loadImageByUrl(@Nullable ImageView imageView, String url) {
        if (TextUtils.isEmpty(url)) return;
        Glide.with(imageView.getContext()).load(url).placeholder(R.mipmap.ic_launcher).
                dontAnimate().into(imageView);
    }

    public static void loadImageEnvelopesByUrl(@Nullable ImageView imageView, String url) {
        if (TextUtils.isEmpty(url)) return;
        Glide.with(imageView.getContext())
                .load(url)
                .placeholder(R.drawable.user_normal)
                .dontAnimate()
                .into(imageView);
    }

    public static void loadUserAvater(@Nullable ImageView imageView, String url){
        if (TextUtils.isEmpty(url)) return;
        Glide.with(imageView.getContext())
                .load(url)
                .signature(new StringSignature(DateUtils.formatDateYYYY_MM_DD(System.currentTimeMillis())))
                .placeholder(R.drawable.user_normal)
                .dontAnimate()
                .transform(new GlideRoundTransform(imageView.getContext()))
                .into(imageView);
    }

    public static void loadImage(@Nullable ImageView imageView, String url) {
        if (TextUtils.isEmpty(url)) return;
        Glide.with(imageView.getContext())
                .load(url)
                .signature(new StringSignature(DateUtils.formatDateYYYY_MM_DD(System.currentTimeMillis())))
                .placeholder(R.drawable.user_normal)
                .dontAnimate()
                .transform(new GlideRoundTransform(imageView.getContext()))
                .into(imageView);
    }

    public static void loadCircleImage(@Nullable ImageView imageView, String url) {
        if (TextUtils.isEmpty(url)) return;
        Glide.with(imageView.getContext())
                .load(url)
                .signature(new StringSignature(url))
                .placeholder(R.drawable.bg_load_img_error)
                .error(R.drawable.bg_load_img_error)
                .dontAnimate()
                .into(imageView);
    }

    /**
     * 加载本地图片
     * @param imageView
     * @param url
     */
    public static void loadLocalImage(@Nullable ImageView imageView, String url) {
        if (TextUtils.isEmpty(url)) return;
        Glide.with(imageView.getContext())
                .load(url)
                .signature(new StringSignature(System.currentTimeMillis()+""))
                .placeholder(R.drawable.user_normal)
                .dontAnimate()
                .into(imageView);
    }

    public static void loadImage1(@Nullable ImageView imageView, String url) {
        if (TextUtils.isEmpty(url)) return;
        Glide.with(imageView.getContext())
                .load(url + "?" + DateUtils.formatDateYYYY_MM_DD(System.currentTimeMillis()))
                //                .placeholder(placeholder)
                .dontAnimate()
                .transform(new GlideRoundTransform(imageView.getContext()))
                .into(imageView);
    }

    public static void reset() {
        iamgeCount = 0;
        loadedCount = 0;
    }

    /**
     * 需要加载的图片数
     */
    private static int iamgeCount = 0;
    /**
     * 已经加载完成的图片数
     */
    private static int loadedCount = 0;

    /**
     * 加载团跑分享页的图片
     */
    public static void loadImage2GroupShare(final Context context, @Nullable final ImageView imageView,
                                            String url, int iamgeCount, @DrawableRes int defResId) {
        if (TextUtils.isEmpty(url)) return;
        DrawableUtils.iamgeCount = iamgeCount;
        Glide.with(context)
                .load(url)
                .asBitmap()
                .transform(new GlideRoundTransform(context))
                .signature(new StringSignature(DateUtils.formatDateYYYY_MM_DD(System.currentTimeMillis())))
                .placeholder(defResId)
                .centerCrop()
                .into(new BitmapImageViewTarget(imageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        imageView.setImageDrawable(circularBitmapDrawable);
                        loadFinish();
                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        super.onLoadFailed(e, errorDrawable);
                        loadFinish();
                    }
                });
    }

    private static void loadFinish() {
        loadedCount++;
        //加载完毕，通知Activity
        if (DrawableUtils.iamgeCount == loadedCount) {
//            EventAction eventAction = new EventAction(EventAction.ACTION_GROUP_SHARE_IMAGE_LOADED);
//            EventBus.getDefault().post(eventAction);
        }
    }

    public static void loadIPortraitByUrl(ImageView imageView, String url) {
        //        .diskCacheStrategy(DiskCacheStrategy.A)
        //        .skipMemoryCache(true)
        if (TextUtils.isEmpty(url)) return;
        if (imageView.getContext() == null) return;
        Glide.with(imageView.getContext())
                .load(url)
                .dontAnimate()//不执行淡入淡出
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .error(R.drawable.user_normal)
                .placeholder(R.drawable.user_normal)
                .into(imageView);
    }

    /**
     * 加载聊天图片
     */
    public static void loadChattingImage(Context context,final ImageView imageView, String url,
                                        final   boolean loadBig, String signature) {

        int hw = loadBig ? SIZE_ORIGINAL : 250;

        if (signature.equals("")) signature = url;

        Glide.with(context)
                .load(url)
                .asBitmap()
                .signature(new StringSignature(System.currentTimeMillis()+""))
                .error(R.drawable.kefuchat_img)
                .placeholder(R.drawable.kefuchat_img)
                .into(new SimpleTarget<Bitmap>(hw, hw) {
                    @Override
                    public void onResourceReady(Bitmap resource,
                                                GlideAnimation<? super Bitmap> glideAnimation) {

                        //加载大图成功
                        if (loadBig) {
                            ToastUtil.getInstance().show("加载大图成功");
                        }

                        imageView.setImageBitmap(resource);
                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        super.onLoadFailed(e, errorDrawable);
                        //加载大图失败
                        if (loadBig) {
                          ToastUtil.getInstance().show("加载大图失败");
                        }
                    }
                });
    }

    /**
     * 加载聊天头像
     */
    public static void loadChattingAvater(Context context, ImageView imageView, Object url) {

        Glide.with(context)
                .load(url)
                .signature(new StringSignature(url + "?1"))
                .transform(new GlideRoundTransform(context, 10))
                .error(R.drawable.user_normal)
                .placeholder(R.drawable.user_normal)
                .into(imageView);
    }

    /**
     * 加载广告
     */
    public static void loadAdvertise(@NonNull ImageView imageView, String url) {
        if (TextUtils.isEmpty(url) || imageView == null || imageView.getContext() == null) return;
        Glide.with(imageView.getContext()).load(url).into(imageView);
    }
}
