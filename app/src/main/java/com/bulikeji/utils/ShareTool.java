package com.bulikeji.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;


import com.bulikeji.App;
import com.bulikeji.R;
import com.bulikeji.com.lzy.okhttputils.utils.log.KLog;
import com.tencent.connect.share.QQShare;
import com.tencent.mm.sdk.constants.Build;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;



/**
 * Created by lujiang on 2016/12/20 0020.
 * 分享工具类
 */

public class ShareTool {
    //分享的类型：图片或者链接
    private Type type = null;
    //分享的渠道
    private Channel channel = null;
    //上下文
    private Activity context = null;
    //分享的文本内容(用于QQ, 短信)
    private String content = null;
    //分享的本地图片路径
    private String localImage = null;
    //分享的链接
    private String url = null;
    //分享的标题（用于QQ）
    private String title = null;
    //分享的远程图片地址（用于QQ）
    private String netImageUrl = null;


    public void setTitle(String title) {
        this.title = title;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public void setContext(Activity context) {
        this.context = context;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setLocalImage(String localImage) {
        this.localImage = localImage;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setNetImageUrl(String netImageUrl) {
        this.netImageUrl = netImageUrl;
    }

    public ShareTool(Activity context) {
        this.context = context;
    }

    @Override
    public String toString() {
        return "ShareTool{" +
                "type=" + type +
                ", channel=" + channel +
                ", context=" + context +
                ", content='" + content + '\'' +
                ", localImage='" + localImage + '\'' +
                ", url='" + url + '\'' +
                ", title='" + title + '\'' +
                ", netImageUrl='" + netImageUrl + '\'' +
                '}';
    }

    public void share() {

        KLog.e("share >>>> " + this.toString());

        if (type == null) {
            ToastUtil.getInstance().show("分享类型不能为空");
            return;
        }

        if (channel == null) {
            ToastUtil.getInstance().show("分享途径不能为空");
            return;
        }

        if (context == null) {
            ToastUtil.getInstance().show("context不能为空");
            return;
        }

        doShare();
    }

    private void doShare() {
        //如果是短信
        if (channel == Channel.SMS) {
            shareSMS();
        } else if (channel == Channel.QQ) {
            shareQQ();
        } else if (channel == Channel.WEIXIN) {
            if (!isInstallWeixin()) {
                ToastUtil.getInstance().show(context.getString(R.string.instalWX));
                return;
            }
            shareWeixin();
        } else if (channel == Channel.WECHAT_MOMENTS) {
            if (!isInstallWeixin()) {
                ToastUtil.getInstance().show(context.getString(R.string.instalWX));
                return;
            }
            shareWechatMoments();
        } else if (channel == Channel.WEIBO) {
            shareWeibo();
        }
    }

    /**
     * 分享到短信
     */
    private void shareSMS() {
        Intent it = new Intent(Intent.ACTION_VIEW);
        it.putExtra("sms_body", content);
        it.setType("vnd.android-dir/mms-sms");
        context.startActivity(it);
    }

    /**
     * 分享到QQ
     */
    private void shareQQ() {

        Tencent mTencent = Tencent.createInstance("100371282", context);

        Bundle params = new Bundle();
        if (type == Type.TYPE_URL) {
            params.putInt(QQShare.SHARE_TO_QQ_KEY_TYPE, QQShare.SHARE_TO_QQ_TYPE_DEFAULT);
            params.putString(QQShare.SHARE_TO_QQ_TARGET_URL, url);
            params.putString(QQShare.SHARE_TO_QQ_IMAGE_URL, netImageUrl);
            params.putString(QQShare.SHARE_TO_QQ_SUMMARY, content);
            params.putString(QQShare.SHARE_TO_QQ_TITLE, title);
        } else {
            params.putString(QQShare.SHARE_TO_QQ_IMAGE_LOCAL_URL, localImage);
            params.putInt(QQShare.SHARE_TO_QQ_KEY_TYPE, QQShare.SHARE_TO_QQ_TYPE_IMAGE);
        }

        params.putString(QQShare.SHARE_TO_QQ_APP_NAME, context.getString(R.string.app_name));

        mTencent.shareToQQ(context, params, new IUiListener() {
            @Override
            public void onComplete(Object o) {

            }

            @Override
            public void onError(UiError uiError) {
                ToastUtil.getInstance().show(uiError.errorMessage);
            }

            @Override
            public void onCancel() {

            }
        });
    }

    /**
     * 分享到微信好友
     */
    private void shareWeixin() {
        Wechat.ShareParams wechatParams = new Wechat.ShareParams();
        if (type == Type.TYPE_URL) {
            wechatParams.setShareType(Platform.SHARE_WEBPAGE);
            wechatParams.url = url;
            wechatParams.title = title;
            wechatParams.text = content;
        } else {
            wechatParams.setImagePath(localImage);
            wechatParams.setShareType(Platform.SHARE_IMAGE);
        }

        Platform platform = ShareSDK.getPlatform(Wechat.NAME);
        platform.share(wechatParams);
    }

    /**
     * 分享到朋友圈
     */
    private void shareWechatMoments() {
        WechatMoments.ShareParams momentsParams = new WechatMoments.ShareParams();
        if (type == Type.TYPE_URL) {
            momentsParams.setShareType(Platform.SHARE_WEBPAGE);
            momentsParams.url = url;
            momentsParams.title = content;
            momentsParams.imageUrl = netImageUrl;
            momentsParams.text = content;
        } else {
            momentsParams.setImagePath(localImage);
            momentsParams.setShareType(Platform.SHARE_IMAGE);
        }

        Platform platform = ShareSDK.getPlatform(WechatMoments.NAME);
        platform.share(momentsParams);
    }

    /**
     * 分享到微博
     */
    private void shareWeibo() {
        SinaWeibo.ShareParams weiboParams = new SinaWeibo.ShareParams();
        if (type == Type.TYPE_URL) {
            weiboParams.setShareType(Platform.SHARE_WEBPAGE);
            weiboParams.text = url;
        } else {
            weiboParams.setImagePath(localImage);
            weiboParams.setShareType(Platform.SHARE_IMAGE);
        }

        Platform weibo = ShareSDK.getPlatform(SinaWeibo.NAME);
        weibo.share(weiboParams);
    }

    private boolean isInstallWeixin() {
        IWXAPI
                sApi = WXAPIFactory.createWXAPI(context, "wx4868b35061f87885");
        return sApi.getWXAppSupportAPI() >= Build.PAY_SUPPORTED_SDK_INT;
    }

    /**
     * 分享方式
     */
    public enum Type {
        TYPE_IMAGE, //图片
        TYPE_URL //链接
    }

    public enum Channel {
        QQ, //QQ
        WEIXIN,//微信好友
        WECHAT_MOMENTS,//朋友圈
        WEIBO,//微博
        SMS,//短信
    }

}
