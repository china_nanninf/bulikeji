package com.bulikeji.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import static com.darsh.multipleimageselect.helpers.Constants.REQUEST_CODE;

/**
 * Created by Administrator on 2017/3/22 0022.
 */

public class CheckPermissionsUtils {
    //是否6.0以上版本
    public static boolean isSDK_M() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) return true;
        return false;
    }

    //是否已经授权 ，
    public static boolean CheckPermissions(Activity Context, String permission) {
        int checkCallPhonePermission =
                ContextCompat.checkSelfPermission(Context, permission);
        if (checkCallPhonePermission != PackageManager.PERMISSION_GRANTED) {

            return false;
        }
        return true;
    }

    public static void requestPsi(Activity Activity, String[] psi, int code) {
        ActivityCompat.requestPermissions(Activity, psi,
                code);
    }
}
