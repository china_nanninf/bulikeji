package com.bulikeji.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

//存储临时信息
public class UserConfigSP {
    private static SharedPreferences preferences;


    public static void setString(Context context, String Key, String Value) {
        System.out.println("value" + Value);
        Tool(context).edit().putString(Key, Value).commit();
    }

    public static String getString(Context context, String Key) {
        return Tool(context).getString(Key, "") == null || Tool(context).getString(Key, "").equals("null") ? "" : Tool(context).getString(Key, "");
    }

    public static int getInt(Context context, String Key) {
        return Tool(context).getInt(Key, 0);
    }

    public static long getLong(Context context, String Key) {
        return Tool(context).getLong(Key, 0);
    }

    @SuppressLint("CommitPrefEdits")
    public static void setBoolean(Context context, boolean b, String key) {
        Tool(context).edit().putBoolean(key, b).commit();
    }

    public static boolean getBoolean(Context context, String key) {
        return Tool(context).getBoolean(key, false);
    }

    public static SharedPreferences Tool(Context context) {
        if (preferences == null) {
            preferences = context.getSharedPreferences(""+UserInfoSharedPreUtils.class.getSimpleName(), Context.MODE_PRIVATE);
        }
        return preferences;
    }

    public void clearAll(Context Context) {
        Tool(Context).edit().clear();
    }
}