package com.bulikeji.utils;

import android.app.Activity;
import android.os.CountDownTimer;
import android.widget.TextView;

import com.bulikeji.R;


public class TimeCountUtils extends CountDownTimer {
  private final Activity activity;
  /**
   * @param millisInFuture    The number of millis in the future from the call to {@link #start()}
   * until the countdown is done and {@link #onFinish()} is called.
   * @param countDownInterval The interval along the way to receive {@link #onTick(long)}
   * callbacks.
   */
  TextView mTextView;

  public TimeCountUtils(Activity Activity, TextView textView, long millisInFuture, long countDownInterval) {
    super(millisInFuture, countDownInterval);
    mTextView = textView;
    activity = Activity;
  }

  @Override
  public void onTick(long millisUntilFinished) {
    mTextView.setText(millisUntilFinished / 1000 + "s");
    mTextView.setEnabled(false);
  }

  @Override
  public void onFinish() {
    mTextView.setText("获取验证码");
    mTextView.setEnabled(true);
  }
}
