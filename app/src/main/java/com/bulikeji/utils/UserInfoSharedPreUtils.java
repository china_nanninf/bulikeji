package com.bulikeji.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;


import com.bulikeji.com.lzy.okhttputils.utils.log.KLog;
import com.bulikeji.ui.entity.ReSetPswEntity;
import com.bulikeji.ui.entity.RegisterEntity;
import com.bulikeji.ui.entity.UserInfoEntity;
import com.bulikeji.ui.login.RegisterActivity;
import com.bulikeji.ui.setting.ReSetPswActivity;

import java.io.File;
import java.util.Map;

/**
 * 用户有关的设置
 */
public class UserInfoSharedPreUtils implements SharedPreferencesImpl {
    public static final String FILE_NAME = UserInfoSharedPreUtils.class.getSimpleName();
    private static volatile UserInfoSharedPreUtils mInstance;

    public static UserInfoSharedPreUtils getInstance() {
        if (mInstance == null) {
            synchronized (SettingSharedPreUtils.class) {
                if (mInstance == null) {
                    mInstance = new UserInfoSharedPreUtils();
                }
            }
        }
        return mInstance;
    }

    private UserInfoSharedPreUtils() {

    }


    public void put(Context context, UserInfoEntity user) {
        UserConfigSP.setString(context, "Name", user.getData().getName() + "");
        UserConfigSP.setString(context, "userId", user.getData().getId() + "");


        UserConfigSP.setString(context, "Thumb", user.getData().getThumb() + "");
        UserConfigSP.setString(context, "Age", user.getData().getAge() + "");
        UserConfigSP.setString(context, "School", user.getData().getSchool() + "");
        UserConfigSP.setString(context, "Signature", user.getData().getSignature() + "");
        UserConfigSP.setString(context, "Phone", user.getData().getPhone() + "");
        UserConfigSP.setString(context, "Code", user.getData().getCode() + "");
        UserConfigSP.setString(context, "CreateTime", user.getData().getCreateTime() + "");
        UserConfigSP.setString(context, "Sex", user.getData().getSex() + "");
        UserConfigSP.setString(context, "deviceNo", user.getData().getDeviceNo() + "");
        UserConfigSP.setString(context, "Token", user.getData().getToken() + "");
        UserConfigSP.setString(context, "Password", user.getData().getPassword() + "");
        KLog.e("AAAAAAAAAAA==" + user.getData().getToken());
    }

    public void put(Context context, RegisterEntity user) {
        UserConfigSP.setString(context, "Name", user.getData().getName() + "");


        UserConfigSP.setString(context, "Thumb", user.getData().getThumb() + "");
        UserConfigSP.setString(context, "Age", user.getData().getAge() + "");
        UserConfigSP.setString(context, "School", user.getData().getSchool() + "");
        UserConfigSP.setString(context, "Signature", user.getData().getSignature() + "");
        UserConfigSP.setString(context, "Phone", user.getData().getPhone() + "");
        UserConfigSP.setString(context, "CreateTime", user.getData().getCreateTime() + "");
        UserConfigSP.setString(context, "Sex", user.getData().getSex() + "");
        UserConfigSP.setString(context, "deviceNo", user.getData().getDeviceNo() + "");
        UserConfigSP.setString(context, "Token", user.getData().getToken() + "");
        UserConfigSP.setString(context, "Password", user.getData().getPassword() + "");
        KLog.e("AAAAAAAAAAA==" + user.getData().getToken());
    }

    public void put(Context context, ReSetPswEntity uploadEntity) {
        UserConfigSP.setString(context, "Name", uploadEntity.getData().getName() + "");


        UserConfigSP.setString(context, "Thumb", uploadEntity.getData().getThumb() + "");
        UserConfigSP.setString(context, "Age", uploadEntity.getData().getAge() + "");
        UserConfigSP.setString(context, "School", uploadEntity.getData().getSchool() + "");
        UserConfigSP.setString(context, "Signature", uploadEntity.getData().getSignature() + "");
        UserConfigSP.setString(context, "Phone", uploadEntity.getData().getPhone() + "");
        UserConfigSP.setString(context, "CreateTime", uploadEntity.getData().getCreateTime() + "");
        UserConfigSP.setString(context, "Sex", uploadEntity.getData().getSex() + "");
        UserConfigSP.setString(context, "deviceNo", uploadEntity.getData().getDeviceNo() + "");
        UserConfigSP.setString(context, "Token", uploadEntity.getData().getToken() + "");
        UserConfigSP.setString(context, "Password", uploadEntity.getData().getPassword() + "");
        KLog.e("AAAAAAAAAAA==" + uploadEntity.getData().getToken());
    }

    public String getDeviceNo(Context context) {
        return UserConfigSP.getString(context, "deviceNo");
    }

    public String getPassword(Context context) {
        return UserConfigSP.getString(context, "Password");
    }

    public String getCreateTime(Context context) {
        return UserConfigSP.getString(context, "CreateTime");
    }

    public Object getSex(Context context) {
        return (Object) UserConfigSP.getString(context, "Sex");
    }

    public Object getPhone(Context context) {
        return UserConfigSP.getString(context, "Phone");
    }


    public Object getSignature(Context context) {
        return (Object) UserConfigSP.getString(context, "Signature");
    }


    public Object getSchool(Context context) {
        return (Object) UserConfigSP.getString(context, "School");
    }

    public Object getAge(Context context) {
        return (Object) UserConfigSP.getString(context, "Age");
    }


    public boolean isLogin(Context Context) {
        return getToken(Context).equals("") || getToken(Context) == null ? false : true;
    }

    public Object getToken(Context context) {
        String token = (String) UserConfigSP.getString(context, "Token") + "";
        KLog.e("AAAAAAAAAAA==" + token);
        return token + "";
    }

    public Object getThumb(Context context) {
        return (Object) UserConfigSP.getString(context, "Thumb");
    }

    public Object getUserId(Context context) {
        return (Object) UserConfigSP.getString(context, "userId");
    }


    public Object getName(Context context) {
        return (Object) UserConfigSP.getString(context, "Name");
    }


    @Override
    public void put(Context context, String key, Object object) {
        SharedPreferenceUtils.put(context, FILE_NAME, key, object);
    }

    @Override
    public Object get(Context context, String key, Object defaultObject) {
        return SharedPreferenceUtils.get(context, FILE_NAME, key, defaultObject);
    }

    @Override
    public void remove(Context context, String key) {
        SharedPreferenceUtils.remove(context, FILE_NAME, key);
    }

    @Override
    public void clear(Context context) {
        UserInfoEntity userInfoEntity = new UserInfoEntity();
        userInfoEntity.setData(new UserInfoEntity.DataBean());
        put(context, userInfoEntity);
        Bitmap bitmap = BitmapFactory.decodeFile(PhotoUtil.getHeadPhotoFileRaw().getAbsolutePath());

        Log.e("PhotoUtil", "setPortrait:" + PhotoUtil.getHeadPhotoFileRaw().getAbsolutePath());

        if (bitmap != null) {
            PhotoUtil.getHeadPhotoFileRaw().delete();
        }
        try {
            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/BL/portrait");
            if (file.exists()) file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public boolean contains(Context context, String key) {
        return SharedPreferenceUtils.contains(context, FILE_NAME, key);
    }

    @Override
    public Map<String, ?> getAll(Context context) {
        return SharedPreferenceUtils.getAll(context, FILE_NAME);
    }

    public boolean IsInputRegisterInfo(Context Context) {
        return (boolean) (UserConfigSP.getString(Context, "Thumb") == null || UserConfigSP.getString(Context, "Thumb").equals("") ? false : true);
    }

    public void setIsInputRegisterInfo(Activity context, boolean b) {
        UserConfigSP.setBoolean(context, b, "is_input_register");
    }


}
