package com.bulikeji.utils;

public class Constants {
  public static final int ACTION_CAMERA = 1;//拍照修改头像
  public static final int ACTION_LOCATION = 2;//本地相册修改头像
  public static final int ACTION_CROP = 3;//系统裁剪头像
}