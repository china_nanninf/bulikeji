package com.bulikeji.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by lujiang on 2016/9/12 0012.
 */
public class MyStatusBarView extends LinearLayout {
  public MyStatusBarView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public MyStatusBarView(Context context) {
    super(context);
  }
}
