package com.bulikeji.utils.photo;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.bulikeji.R;
import com.bulikeji.base.BaseActivity;
import com.bulikeji.receiver.ReceiverUtil;
import com.bulikeji.utils.ToastUtil;

import java.util.ArrayList;
import java.util.List;

import static android.widget.Toast.LENGTH_LONG;

public class PhotosActivity extends BaseActivity implements View.OnClickListener {

    private PhotosFragment pickerFragment;
    private ImagePagerFragment imagePagerFragment;

    public final static String EXTRA_MAX_COUNT = "MAX_COUNT";
    public final static String EXTRA_SHOW_CAMERA = "SHOW_CAMERA";
    public final static String EXTRA_SHOW_GIF = "SHOW_GIF";
    public final static String KEY_SELECTED_PHOTOS = "SELECTED_PHOTOS";


    public final static int DEFAULT_MAX_COUNT = 9;

    private int maxCount = DEFAULT_MAX_COUNT;

    /**
     * to prevent multiple calls to inflate menu
     */
    private boolean menuIsInflated = false;

    private boolean showGif = false;

    private TextView cancel, selectPhotos;
    private Button comfirm;


    /**
     * Overriding this method allows us to run our exit animation first, then exiting
     * the activity when it complete.
     */
    @Override
    public void onBackPressed() {
        if (imagePagerFragment != null && imagePagerFragment.isVisible()) {
            imagePagerFragment.runExitAnimation(new Runnable() {
                public void run() {
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                        getSupportFragmentManager().popBackStack();
                    }
                }
            });
        } else {
            super.onBackPressed();
        }
    }


    public void addImagePagerFragment(ImagePagerFragment imagePagerFragment) {
        this.imagePagerFragment = imagePagerFragment;
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, this.imagePagerFragment)
                .addToBackStack(null)
                .commit();
    }


    public PhotosActivity getActivity() {
        return this;
    }

    public boolean isShowGif() {
        return showGif;
    }

    public void setShowGif(boolean showGif) {
        this.showGif = showGif;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void setListener() {

    }

    @Override
    protected void initTitle() {

    }

    @Override
    protected void initView() {
        boolean showCamera = getIntent().getBooleanExtra(EXTRA_SHOW_CAMERA, true);
        boolean showGif = getIntent().getBooleanExtra(EXTRA_SHOW_GIF, false);
        setShowGif(showGif);


        cancel = (TextView) findViewById(R.id.cancel);
        selectPhotos = (TextView) findViewById(R.id.select_photos);
        comfirm = (Button) findViewById(R.id.comfirm);
        cancel.setOnClickListener(this);
        selectPhotos.setOnClickListener(this);
        comfirm.setOnClickListener(this);


        maxCount = getIntent().getIntExtra(EXTRA_MAX_COUNT, DEFAULT_MAX_COUNT);

        pickerFragment = (PhotosFragment) getSupportFragmentManager().findFragmentById(R.id.photoPickerFragment);

        pickerFragment.getPhotoGridAdapter().setShowCamera(showCamera);

        pickerFragment.getPhotoGridAdapter().setOnItemCheckListener(new OnItemCheckListener() {
            @Override
            public boolean OnItemCheck(int position, Photo photo, final boolean isCheck, int selectedItemCount) {

                int total = selectedItemCount + (isCheck ? -1 : 1);

                comfirm.setText(total == 0 ? "确定" : "确定(0/1)");

                if (maxCount <=0) {
                    List<Photo> photos = pickerFragment.getPhotoGridAdapter().getSelectedPhotos();
                    if (!photos.contains(photo)) {
                        photos.clear();
                        pickerFragment.getPhotoGridAdapter().notifyDataSetChanged();
                    }
                    return true;
                }

                if (total > maxCount) {
                    ToastUtil.getInstance().show(getString(R.string.picker_over_max_count_tips, maxCount));
                    return false;
                }
                comfirm.setText(getString(R.string.picker_select_photos_count, total, maxCount));
                return true;
            }
        });
    }

    @Override
    protected boolean isApplyReceiver() {
        return true;
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_photos;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel:
                finish();
                break;
            case R.id.select_photos:
                ArrayList<String> selectedPhoto = pickerFragment.getPhotoGridAdapter().getSelectedPhotoPaths();
                if (selectedPhoto.size()==0){
                    ToastUtil.getInstance().show("你还没有选择呢");
                    return;
                }
                String s = selectedPhoto.get(0).toString();
                Result(s);

                break;
            case R.id.comfirm:

                ArrayList<String> selectedPhotos = pickerFragment.getPhotoGridAdapter().getSelectedPhotoPaths();
                if (selectedPhotos.size()==0){
                    ToastUtil.getInstance().show("你还没有选择呢");
                    return;
                }
                String sPhotos = selectedPhotos.get(0).toString();
                Result(sPhotos);
                break;
        }

    }

    private void Result(String sPhotos) {
        ReceiverUtil.SendEvent(this,ReceiverUtil.Contans.SELECT_PHOTO,sPhotos);
        finish();
    }


}
