package com.bulikeji.utils.photo;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;


/**
 * Created by donglua on 15/7/2.
 */
public class PhotosIntent extends Intent {
    public static void startPhoto(Context Context) {
        PhotosIntent intent = new PhotosIntent(Context);
        intent.setPhotoCount(5);
        intent.setShowCamera(false);
        Context.startActivity(intent);
    }

    private PhotosIntent() {
    }

    private PhotosIntent(Intent o) {
        super(o);
    }

    private PhotosIntent(String action) {
        super(action);
    }

    private PhotosIntent(String action, Uri uri) {
        super(action, uri);
    }

    private PhotosIntent(Context packageContext, Class<?> cls) {
        super(packageContext, cls);
    }

    public PhotosIntent(Context packageContext) {
        super(packageContext, PhotosActivity.class);
    }

    public void setPhotoCount(int photoCount) {
        this.putExtra(PhotosActivity.EXTRA_MAX_COUNT, photoCount);
    }

    public void setShowCamera(boolean showCamera) {
        this.putExtra(PhotosActivity.EXTRA_SHOW_CAMERA, showCamera);
    }


}
