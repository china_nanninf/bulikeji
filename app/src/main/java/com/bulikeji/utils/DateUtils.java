/*
 *   Copyright (C)  2016 android@19code.com
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.bulikeji.utils;

import android.util.Log;


import com.bulikeji.ui.entity.Global;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Create by h4de5ing 2016/5/7 007
 */
public class DateUtils {
    private static final SimpleDateFormat DATE_FORMAT_DATETIME =
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final SimpleDateFormat DATE_FORMAT_DATE_YYYY_MM_DD_HH =
            new SimpleDateFormat("yyyy-MM-dd-HH");
    private static final SimpleDateFormat DATE_FORMAT_DATE = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat DATE_FORMAT_DATE_MM_DD_HH_MM =
            new SimpleDateFormat("MM-dd HH:mm");
    private static final SimpleDateFormat DATE_FORMAT_DATE_HH_MM =
            new SimpleDateFormat("HH:mm");
    private static final SimpleDateFormat DATE_FORMAT_DATE_MM_DD_HH_MM_ =
            new SimpleDateFormat("MM月dd日 HH:mm");
    private static final SimpleDateFormat DATE_FORMAT_DATE_DD_HH_MM=
            new SimpleDateFormat("dd日 HH:mm");
    private static final SimpleDateFormat DATE_FORMAT_DATE_MM=
            new SimpleDateFormat("M月");
    private static final SimpleDateFormat DATE_FORMAT_DATE_YYYY_MM = new SimpleDateFormat("yyyy-MM");
    private static final SimpleDateFormat DATE_FORMAT_TIME = new SimpleDateFormat("HH:mm:ss");
    public static final String FORMAT_HTTP_DATA = "EEE, dd MMM y HH:mm:ss 'GMT'";
    public static final SimpleDateFormat formatter =
            new SimpleDateFormat(FORMAT_HTTP_DATA, Locale.US);
    public static final TimeZone GMT_TIME_ZONE = TimeZone.getTimeZone("GMT");

    /**
     * 计算两个日期之间相差的天数
     */
    public static int diffDays(long Millis, long Millis2) {
        long dy = Millis - Millis2;
        long diffDays = dy / (24 * 60 * 60 * 1000l);
        return (int) Math.abs(diffDays);
    }

    /**
     * 计算这个时间和服务器时间差多少
     */
    public static int diffDays(long Millis) {
        if (Global.CurrentServerTimeMillis == 0) {
            Global.CurrentServerTimeMillis = System.currentTimeMillis();
        }
        long dy = Millis - Global.CurrentServerTimeMillis;
        long diffDays = dy / (24 * 60 * 60 * 1000l);
        return (int) Math.abs(diffDays);
    }

    /**
     * @return 0.00的时间
     */
    private static long getZeroTime(long Millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Millis);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTimeInMillis();
    }

    /**
     * Parsing the TimeZone of time in milliseconds.
     *
     * @param gmtTime GRM Time, Format such as: {@value #FORMAT_HTTP_DATA}.
     * @return The number of milliseconds from 1970.1.1.
     * @throws ParseException if an error occurs during parsing.
     */
    public static long parseGMTToMillis(String gmtTime) throws ParseException {
        formatter.setTimeZone(GMT_TIME_ZONE);
        Date date = formatter.parse(gmtTime);
        return date.getTime();
    }

    public static String formatDataTime(long date) {
        return DATE_FORMAT_DATETIME.format(new Date(date));
    }

    public static String datetime() {
        return DATE_FORMAT_DATETIME.format(new Date());
    }


    public static String formatDate(long date) {
        return DATE_FORMAT_DATE.format(new Date(date));
    }

    public static String hourAndMinute(long date) {
        return DATE_FORMAT_DATE_HH_MM.format(new Date(date));
    }

    public static String formatRedRecordDate(long date) {
        return DATE_FORMAT_DATE_MM_DD_HH_MM.format(new Date(date));
    }

    public static String formatDateYYYY_MM(long date) {
        return DATE_FORMAT_DATE_YYYY_MM.format(new Date(date));
    }

    public static String formatDateYYYY_MM_DD_HH(long date) {
        return DATE_FORMAT_DATE_YYYY_MM_DD_HH.format(new Date(date));
    }

    public static String formatDateYYYY_MM_DD(long date) {
        return DATE_FORMAT_DATE.format(new Date(date));
    }

    public static String formatDateMM_DD_HH_MM(long date) {
        return DATE_FORMAT_DATE_MM_DD_HH_MM.format(new Date(date));
    }

    /**
     * 格式：23日 16:30
     * @param date
     * @return
     */
    public static String formatDateDD(long date) {
        return DATE_FORMAT_DATE_DD_HH_MM.format(new Date(date));
    }


    /**
     * 格式：12月
     * @param date
     * @return
     */
    public static String formatDateMM(long date) {
        return DATE_FORMAT_DATE_MM.format(new Date(date));
    }
    public static String formatDateMM_DD_HH_MM_(long date) {
        return DATE_FORMAT_DATE_MM_DD_HH_MM_.format(new Date(date));
    }

    public static String formatTime(long date) {
        return DATE_FORMAT_TIME.format(new Date(date));
    }

    public static String formatDateCustom(String beginDate, String format) {
        return new SimpleDateFormat(format).format(new Date(Long.parseLong(beginDate)));
    }

    public static String formatDateCustom(Date beginDate, String format) {
        return new SimpleDateFormat(format).format(beginDate);
    }

    public static Date string2Date(String s, String style) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern(style);
        Date date = null;
        if (s == null || s.length() < 6) {
            return null;
        }
        try {
            date = simpleDateFormat.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getTime() {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        return cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(
                Calendar.SECOND);
    }

    public static long subtractDate(Date dateStart, Date dateEnd) {
        return dateEnd.getTime() - dateStart.getTime();
    }

    public static Date getDateAfter(Date d, int day) {
        Calendar now = Calendar.getInstance();
        now.setTime(d);
        now.set(Calendar.DATE, now.get(Calendar.DATE) + day);
        return now.getTime();
    }

    public static int getWeekOfMonth() {
        Calendar calendar = Calendar.getInstance();
        int week = calendar.get(Calendar.WEEK_OF_MONTH);
        return week - 1;
    }

    public static int getDayOfWeek() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        if (day == 1) {
            day = 7;
        } else {
            day = day - 1;
        }
        return day;
    }

    /**
     * 计算两个日期之间相差的天数
     */
    public static int daysBetween(Date date1, Date date2) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);
        long time1 = cal.getTimeInMillis();
        cal.setTime(date2);
        long time2 = cal.getTimeInMillis();
        long between_days = (time2 - time1) / (1000 * 3600 * 24);
        Log.e("GroupDetailActivity", "daysBetween:" + String.valueOf(between_days));
        return Integer.parseInt(String.valueOf(between_days)) + 1;
    }
}
