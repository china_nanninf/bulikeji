package com.bulikeji.acp;

import android.content.Context;

/**
 * Created by hupei on 2016/4/26.
 */
public class Acp {

  private static Acp mInstance;
  private com.bulikeji.acp.AcpManager mAcpManager;

  public static Acp getInstance(Context context) {
    if (mInstance == null) {
      synchronized (Acp.class) {
        if (mInstance == null) {
          mInstance = new Acp(context);
        }
      }
    }
    return mInstance;
  }

  private Acp(Context context) {
    mAcpManager = new AcpManager(context.getApplicationContext());
  }

  /**
   * 开始请求
   */
  public void request(AcpOptions options, AcpListener acpListener) {
    if (options == null) new NullPointerException("AcpOptions is null...");
    if (acpListener == null) new NullPointerException("AcpListener is null...");
    mAcpManager.request(options, acpListener);
  }

  AcpManager getAcpManager() {
    return mAcpManager;
  }
}
