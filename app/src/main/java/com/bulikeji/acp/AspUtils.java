package com.bulikeji.acp;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;


import com.bulikeji.R;


import static com.darsh.multipleimageselect.helpers.Constants.REQUEST_CODE;
public class AspUtils {

  /**
   * 检测权限
   */
  public static void checkprimissio(final Activity activity, AspListener mListener) {
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
      if (Settings.canDrawOverlays(activity)) {
        if (mListener == null) return;
        mListener.onto();
      } else {
        new AlertDialog.Builder(activity).setMessage(activity.getString(R.string.MustAuthorize))
            .setCancelable(false)
            .setNegativeButton("关闭", new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialogInterface, int i) {
                activity.finish();
                return;
              }
            })

            .setPositiveButton(activity.getString(R.string.SetPermissions),
                new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialog, int which) {
                    requestAlertWindowPermission(activity);
                  }
                })
            .show();
      }
    } else {
      if (mListener == null) return;
      mListener.onto();
    }
  }

  /**
   * 检测权限
   */
  public static void checkprimissio2( final  Activity activity, AspListener mListener) {
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
      if (Settings.canDrawOverlays(activity)) {
        if (mListener == null) return;
        mListener.onto();
      } else {
        new AlertDialog.Builder(activity).setMessage(activity.getString(R.string.MustAuthorize))
            .setCancelable(false)
            .setNegativeButton("关闭", new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialogInterface, int i) {
                return;
              }
            })

            .setPositiveButton(activity.getString(R.string.SetPermissions),
                new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialog, int which) {
                    requestAlertWindowPermission(activity);
                  }
                })
            .show();
      }
    } else {
      if (mListener == null) return;
      mListener.onto();
    }
  }

  private static void requestAlertWindowPermission(Activity activity) {
    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
    intent.setData(Uri.parse("package:" + activity.getPackageName()));
    activity.startActivityForResult(intent,REQUEST_CODE);
  }

  private static String getString(@StringRes int res) {
    return Resources.getSystem().getString(res);
  }
}
