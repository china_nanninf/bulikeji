package com.bulikeji.ui.setting;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bulikeji.App;
import com.bulikeji.MainActivity;
import com.bulikeji.R;
import com.bulikeji.acp.Acp;
import com.bulikeji.acp.AcpOptions;
import com.bulikeji.base.BaseActivity;
import com.bulikeji.com.lzy.okhttputils.utils.log.KLog;
import com.bulikeji.net.HttpManager;
import com.bulikeji.net.callback.BaseCallback;
import com.bulikeji.receiver.ReceiverUtil;
import com.bulikeji.ui.entity.UploadEntity;
import com.bulikeji.ui.entity.UserInfoEntity;
import com.bulikeji.ui.login.RegisterActivity;
import com.bulikeji.utils.ActionSheetDialog;
import com.bulikeji.utils.AppInfo;
import com.bulikeji.utils.CheckPermissionsUtils;
import com.bulikeji.utils.DrawableUtils;
import com.bulikeji.utils.PhoneInfo;
import com.bulikeji.utils.PhotoUtil;
import com.bulikeji.utils.StringUtils;
import com.bulikeji.utils.ToastUtil;
import com.bulikeji.utils.UserInfoSharedPreUtils;
import com.bulikeji.utils.de.hdodenhof.circleimageview.CircleImageView;
import com.bulikeji.utils.photo.PhotosIntent;
import com.bulikeji.utils.photov2.CropUtils;
import com.bulikeji.utils.photov2.DialogPermission;
import com.bulikeji.utils.photov2.FileUtil;
import com.bulikeji.utils.photov2.PermissionUtil;
import com.bulikeji.utils.photov2.SharedPreferenceMark;
import com.bulikeji.widget.TitleBarView;
import com.hyphenate.chatui.DemoHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import static com.bulikeji.utils.Constants.ACTION_CAMERA;
import static com.bulikeji.utils.Constants.ACTION_CROP;
import static com.bulikeji.utils.Constants.ACTION_LOCATION;
import static com.darsh.multipleimageselect.helpers.Constants.REQUEST_CODE;

public class EditUserInfoActivity extends BaseActivity {
    private android.widget.RelativeLayout activityRegister;
    private com.bulikeji.widget.TitleBarView titlew;
    private com.bulikeji.utils.de.hdodenhof.circleimageview.CircleImageView userIcon;
    private android.widget.TextView selectIcon;
    private android.widget.EditText inputRegisterName;
    private android.widget.Button btW;
    private android.widget.Button btM;
    private android.widget.EditText inputRegisterAge;
    private android.widget.EditText inputRegisterSchool;
    private android.widget.EditText inputUserSig;

    private String path = (String) UserInfoSharedPreUtils.getInstance().getThumb(this);
    private String name = (String) UserInfoSharedPreUtils.getInstance().getName(this);
    private String age = (String) UserInfoSharedPreUtils.getInstance().getAge(this);
    private String school = (String) UserInfoSharedPreUtils.getInstance().getSchool(this);
    private String sex = (String) UserInfoSharedPreUtils.getInstance().getSex(this);
    private String sig = (String) UserInfoSharedPreUtils.getInstance().getSignature(this);
    private boolean sex1;
    private String coverPath = (String) UserInfoSharedPreUtils.getInstance().getThumb(this);
    private boolean is_change;
    private int acp_code = 0;
    private String imsi = "";
    private boolean is_change_icon;
    private int isWRcode;
    private Bitmap bitmap;
    private boolean is_select_photo;
    private File file;
    private Uri uri;
    private Bitmap bitmap1;

    public static void start(Context Context) {
        Context.startActivity(new Intent(Context, EditUserInfoActivity.class));
    }

    @Override
    protected void initData() {

        inputRegisterAge.setText(age + "");
        inputRegisterSchool.setText(school + "");
        inputRegisterName.setText(name + "");
        inputUserSig.setText(sig);
        DrawableUtils.loadCircleImage(userIcon, path);
    }

    public void getIemi() {

        imsi = AppInfo.getModelId(this);
    }

    @Override
    protected void setListener() {
        btM.setOnClickListener(this);
        btW.setOnClickListener(this);
        selectIcon.setOnClickListener(this);
        userIcon.setOnClickListener(this);


    }

    @Override
    protected void initTitle() {
        titlew.setTitle("编辑资料");
        titlew.setTitleR("保存", this);
    }

    @Override
    protected void initView() {

        activityRegister = (RelativeLayout) findViewById(R.id.activity_register);
        titlew = (TitleBarView) findViewById(R.id.titlew);
        userIcon = (CircleImageView) findViewById(R.id.user_icon);
        selectIcon = (TextView) findViewById(R.id.select_icon);
        inputRegisterName = (EditText) findViewById(R.id.input_register_name);
        btW = (Button) findViewById(R.id.bt_w);
        btM = (Button) findViewById(R.id.bt_m);
        inputRegisterAge = (EditText) findViewById(R.id.input_register_age);
        inputRegisterSchool = (EditText) findViewById(R.id.input_register_school);
        inputUserSig = (EditText) findViewById(R.id.input_user_sig);
        initStyle(sex.equals("女") ? true : false);
        init();


    }

    private static final int REQUEST_CODE_TAKE_PHOTO = 1;
    private static final int REQUEST_CODE_ALBUM = 2;
    private static final int REQUEST_CODE_CROUP_PHOTO = 3;

    private void init() {
        file = new File(FileUtil.getCachePath(this), "user-avatar.jpg");
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            uri = Uri.fromFile(file);
        } else {
            //通过FileProvider创建一个content类型的Uri(android 7.0需要这样的方法跨应用访问)
            uri = FileProvider.getUriForFile(App.getInstance(), "com.bulikeji.fileProvider", file);
        }
    }

    private void initStyle(boolean b) {
        sex1 = b;
        if (b) {
            btM.setSelected(false);
            btW.setSelected(true);
            btW.setTextColor(getResources().getColor(R.color.white));
            btM.setTextColor(getResources().getColor(R.color.black));

            return;
        }
        btW.setSelected(false);
        btM.setSelected(true);
        btM.setTextColor(getResources().getColor(R.color.white));
        btW.setTextColor(getResources().getColor(R.color.black));

    }

    private void chackPsi() {
        //是6.0    没有授权
        if (CheckPermissionsUtils.isSDK_M()) {
            if (!CheckPermissionsUtils.CheckPermissions(this, Manifest.permission.READ_PHONE_STATE)) {
                CheckPermissionsUtils.requestPsi(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 3);

            }
            if (CheckPermissionsUtils.CheckPermissions(this, Manifest.permission.READ_PHONE_STATE))
                getIemi();


        }
        if (!CheckPermissionsUtils.isSDK_M()) getIemi();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {
            case R.id.title_r:

                age = inputRegisterAge.getText().toString().trim();
                name = inputRegisterName.getText().toString().trim();
                school = inputRegisterSchool.getText().toString().trim();
                sig = inputUserSig.getText().toString().trim();

                if (StringUtils.isEmpty(name)) {
                    ToastUtil.getInstance().show("请输入名称");
                    return;
                }
                if (name.length() > 6 || name.length() < 2) {
                    ToastUtil.getInstance().show("名称格式为2-6位");
                    return;
                }
                if (StringUtils.isEmpty(age)) {
                    ToastUtil.getInstance().show("请输入Age");
                    return;
                }
                if (StringUtils.isEmpty(sig)) {
                    ToastUtil.getInstance().show("请输入签名");
                    return;
                }
                if (StringUtils.isEmpty(school)) {
                    ToastUtil.getInstance().show("请输入School");
                    return;
                }
                if (StringUtils.isEmpty(coverPath)) {
                    ToastUtil.getInstance().show("请选择头像");
                    return;
                }
                sex = sex1 ? "女" : "男";
                if (!coverPath.equals(UserInfoSharedPreUtils.getInstance().getThumb(this)) || !age.equals(UserInfoSharedPreUtils.getInstance().getAge(this)) ||
                        !school.equals(UserInfoSharedPreUtils.getInstance().getSchool(this)) || !sig.equals(UserInfoSharedPreUtils.getInstance().getSignature(this))
                        || !name.equals(UserInfoSharedPreUtils.getInstance().getName(this)) || !sex.equals(UserInfoSharedPreUtils.getInstance().getSex(this))) {
                    is_change = true;

                }
                if (!is_change) {
                    ToastUtil.getInstance().show("你还没有做出修改");
                    return;
                }
                chackPsi();

                UserInfoEntity UserInfoEntity = new UserInfoEntity();
                UserInfoEntity.setData(new UserInfoEntity.DataBean());
                UserInfoEntity.getData().setName(name);
                UserInfoEntity.getData().setThumb(coverPath);
                UserInfoEntity.getData().setAge(Integer.valueOf(age));
                UserInfoEntity.getData().setSex(sex);
                UserInfoEntity.getData().setSchool(school);
                UserInfoEntity.getData().setPhone((String) UserInfoSharedPreUtils.getInstance().getPhone(this));
                UserInfoEntity.getData().setDeviceNo(imsi);
                UserInfoEntity.getData().setToken((String) UserInfoSharedPreUtils.getInstance().getToken(this));
                UserInfoEntity.getData().setSignature(sig);
                is_change_icon = true;
                UserInfoSharedPreUtils.getInstance().put(EditUserInfoActivity.this, UserInfoEntity);
                ToastUtil.getInstance().show("修改资料完成");
                UserInfoSharedPreUtils.getInstance().setIsInputRegisterInfo(EditUserInfoActivity.this, true);
                DemoHelper.getInstance().getUserProfileManager().updateCurrentUserNickName(UserInfoEntity.getData().getName());
                if (bitmap1!=null)
                    DemoHelper.getInstance().getUserProfileManager().uploadUserAvatar(DemoHelper.Bitmap2Bytes(bitmap1));
                ReceiverUtil.SendEvent(EditUserInfoActivity.this, ReceiverUtil.Contans.UPDATE_USERINF);
                finish();
//                HttpManager.update(UserInfoEntity, this, new BaseCallback<UserInfoEntity>() {
//                    @Override
//                    public void onLogicSuccess(UserInfoEntity userInfoEntity) {
//                        super.onLogicSuccess(userInfoEntity);
//                        UserInfoSharedPreUtils.getInstance().put(EditUserInfoActivity.this, userInfoEntity);
//                        ToastUtil.getInstance().show("修改资料完成");
//                        UserInfoSharedPreUtils.getInstance().setIsInputRegisterInfo(EditUserInfoActivity.this, true);
//                        DemoHelper.getInstance().getUserProfileManager().updateCurrentUserNickName(userInfoEntity.getData().getName());
//                        if (bitmap1!=null)
//                        DemoHelper.getInstance().getUserProfileManager().uploadUserAvatar(DemoHelper.Bitmap2Bytes(bitmap1));
//                        ReceiverUtil.SendEvent(EditUserInfoActivity.this, ReceiverUtil.Contans.UPDATE_USERINF);
//                        finish();
//                    }
//
//                    @Override
//                    public void onLogicFailure(UserInfoEntity userInfoEntity) {
//                        super.onLogicFailure(userInfoEntity);
//                        ToastUtil.getInstance().show(userInfoEntity.ret_msg);
//                    }
//                });


                break;
            case R.id.select_icon:
            case R.id.user_icon:
                new ActionSheetDialog(this).builder()
                        .setCancelable(false)
                        .setCanceledOnTouchOutside(false)
                        .addSheetItem("从相册选一张", ActionSheetDialog.SheetItemColor.Blue,
                                new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {
                                        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                                            ToastUtil.getInstance().show("SD不可用");
                                            return;
                                        }
                                        if (PermissionUtil.hasCameraPermission(EditUserInfoActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE, PermissionUtil.REQUEST_WRITE_EXTERNAL_STORAGE)
                                                && PermissionUtil.hasCameraPermission(EditUserInfoActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE, PermissionUtil.REQUEST_READ_EXTERNAL_STORAGE)) {

                                            choose();
                                        }
                                    }
                                })
                        .addSheetItem("拍一张相片", ActionSheetDialog.SheetItemColor.Blue,
                                new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {
                                        if (PermissionUtil.hasCameraPermission(EditUserInfoActivity.this, Manifest.permission.CAMERA, PermissionUtil.REQUEST_SHOWCAMERA)) {
                                            take();
                                        }

                                    }
                                })
                        .show();
                break;
            case R.id.bt_m:
                sex1 = false;
                initStyle(sex1);
                break;
            case R.id.bt_w:
                sex1 = true;
                initStyle(sex1);
                break;
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {

            case PermissionUtil.REQUEST_SHOWCAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    take();

                } else {
                    if (!SharedPreferenceMark.getHasShowCamera()) {
                        SharedPreferenceMark.setHasShowCamera(true);
                        new DialogPermission(this, "关闭摄像头权限影响扫描功能,请去管家->权限管理，设置权限");

                    } else {
                        Toast.makeText(this, "未获取摄像头权限", Toast.LENGTH_SHORT)
                                .show();
                    }
                }
                break;
            case PermissionUtil.REQUEST_READ_EXTERNAL_STORAGE:
            case PermissionUtil.REQUEST_WRITE_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    choose();

                } else {
                    if (!SharedPreferenceMark.getHasShowCamera()) {
                        SharedPreferenceMark.setHasShowExtralStoreage(true);
                        new DialogPermission(this, "关闭读写权限影响相册功能,请去管家->权限管理，设置权限");

                    } else {
                        Toast.makeText(this, "未获取读写权限", Toast.LENGTH_SHORT)
                                .show();
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        if (requestCode == 3) {
            int checkCallPhonePermission =
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
            if (checkCallPhonePermission != PackageManager.PERMISSION_GRANTED) {
                ToastUtil.getInstance().show("请在手机管家授予唯一标识权限");
                return;
            }
            getIemi();
        }


    }

    private void choose() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_ALBUM);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != -1) {
            return;
        }
        if (requestCode == REQUEST_CODE_ALBUM && data != null) {
            Uri newUri;
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                newUri = Uri.parse("file:///" + CropUtils.getPath(this, data.getData()));
            } else {
                newUri = data.getData();
            }
            if (newUri != null) {
                startPhotoZoom(newUri);
            } else {
                Toast.makeText(this, "没有得到相册图片", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == REQUEST_CODE_TAKE_PHOTO) {
            startPhotoZoom(uri);
        } else if (requestCode == REQUEST_CODE_CROUP_PHOTO) {
            uploadAvatarFromPhoto();

        }


    }

    private void uploadAvatarFromPhoto() {

        coverPath = file.getPath();
        try {


            bitmap1 = BitmapFactory.decodeStream(new FileInputStream(new File(coverPath)));

            userIcon.setImageBitmap(bitmap1);
            userIcon.invalidate();
            chackPsi();
            uploadIcon(new File(coverPath), imsi);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 裁剪拍照裁剪
     *
     * @param uri
     */
    public void startPhotoZoom(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra("crop", "true");// crop=true 有这句才能出来最后的裁剪页面.
        intent.putExtra("aspectX", 1);// 这两项为裁剪框的比例.
        intent.putExtra("aspectY", 1);// x:y=1:1
//        intent.putExtra("outputX", 400);//图片输出大小
//        intent.putExtra("outputY", 400);
        intent.putExtra("output", Uri.fromFile(file));
        intent.putExtra("outputFormat", "JPEG");// 返回格式
        startActivityForResult(intent, REQUEST_CODE_CROUP_PHOTO);
    }

    private void uploadIcon(File file, String iemi) {
        HttpManager.uploadIcon(file, iemi, this, new BaseCallback<UploadEntity>() {

            @Override
            public void onLogicFailure(UploadEntity uploadEntity) {
                super.onLogicFailure(uploadEntity);
                ToastUtil.getInstance().show(uploadEntity.ret_msg);
                PhotoUtil.setPortrait(userIcon, EditUserInfoActivity.this);
            }

            @Override
            public void onLogicSuccess(UploadEntity uploadEntity) {
                super.onLogicSuccess(uploadEntity);
                coverPath = uploadEntity.getData() + "";
                if (!StringUtils.isEmpty(coverPath)) {
                    DrawableUtils.loadCircleImage(userIcon, coverPath);
                    userIcon.invalidate();
                }
            }
        });

    }


    private void take() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra(MediaStore.Images.Media.ORIENTATION, 0);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent, REQUEST_CODE_TAKE_PHOTO);
    }


    @Override
    protected boolean isApplyReceiver() {
        return true;
    }

    @Override
    public void onEvent(Intent intent) {
        super.onEvent(intent);


    }

    @Override
    protected int getContentView() {
        return R.layout.activity_edit_user_info;
    }

    @Override
    public void onGranted() {


        getIemi();


    }

    @Override
    public void onDenied(List<String> permissions) {
        ToastUtil.getInstance().show("请在手机管家授予读写 唯一标识 相机 权限");
    }


}
