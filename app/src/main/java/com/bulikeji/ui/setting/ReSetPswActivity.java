package com.bulikeji.ui.setting;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bulikeji.R;
import com.bulikeji.base.BaseActivity;
import com.bulikeji.net.HttpManager;
import com.bulikeji.net.callback.BaseCallback;
import com.bulikeji.ui.entity.ReSetPswEntity;
import com.bulikeji.ui.entity.SmsCodeEntity;
import com.bulikeji.ui.entity.UploadEntity;
import com.bulikeji.ui.entity.UserInfoEntity;
import com.bulikeji.utils.AccountDetectionUtils;
import com.bulikeji.utils.AppInfo;
import com.bulikeji.utils.CheckPermissionsUtils;
import com.bulikeji.utils.PhoneInfo;
import com.bulikeji.utils.StringUtils;
import com.bulikeji.utils.TimeCountUtils;
import com.bulikeji.utils.ToastUtil;
import com.bulikeji.utils.UserInfoSharedPreUtils;
import com.bulikeji.widget.TitleBarView;

public class ReSetPswActivity extends BaseActivity {

    private com.bulikeji.widget.TitleBarView titlew;
    private android.widget.TextView bg;
    private android.widget.RelativeLayout rlResetInfo;
    private android.widget.EditText inputResetPhone;
    private android.widget.EditText inputResetCode;
    private android.widget.Button resetCodeClick;
    private android.widget.RelativeLayout rlResetPsw;
    private android.widget.EditText inputPsw;
    private android.widget.EditText inputResPsw;
    private android.widget.Button resetClick;
    private String ResetPhone;
    private String ResetCode;
    private String sms_code;
    private String Psw;
    private String ResPsw;
    private TimeCountUtils mTimeCountUtils;
    private String imei;

    public static void start(Context Context) {
        Context.startActivity(new Intent(Context, ReSetPswActivity.class));
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void setListener() {
        resetClick.setOnClickListener(this);
        resetCodeClick.setOnClickListener(this);
    }

    @Override
    protected void initTitle() {
        titlew.setTitle("重置密码");

    }

    @Override
    protected void initView() {

        titlew = (TitleBarView) findViewById(R.id.titlew);
        bg = (TextView) findViewById(R.id.bg);
        rlResetInfo = (RelativeLayout) findViewById(R.id.rl_reset_info);
        inputResetPhone = (EditText) findViewById(R.id.input_reset_phone);
        inputResetCode = (EditText) findViewById(R.id.input_reset_code);
        resetCodeClick = (Button) findViewById(R.id.reset_code_click);
        rlResetPsw = (RelativeLayout) findViewById(R.id.rl_reset_psw);
        inputPsw = (EditText) findViewById(R.id.input_psw);
        inputResPsw = (EditText) findViewById(R.id.input_res_psw);
        resetClick = (Button) findViewById(R.id.reset_click);
        mTimeCountUtils = new TimeCountUtils(this, resetCodeClick, 60 * 1000, 1 * 1000);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.reset_code_click:
                String RegisterPhones = inputResetPhone.getText().toString().trim() + "";
                if (StringUtils.isEmpty(RegisterPhones)) {
                    ToastUtil.getInstance().show("请输入手机号");
                    return;
                }
                if (AccountDetectionUtils.isMobileNO110(RegisterPhones)) {
                    ToastUtil.getInstance().show("手机格式不正确");
                    return;
                }
                getCode(RegisterPhones);
                break;
            case R.id.reset_click:

                ResetPhone = inputResetPhone.getText().toString().trim() + "";
                ResetCode = inputResetCode.getText().toString().trim() + "";
                Psw = inputPsw.getText().toString().trim() + "";
                ResPsw = inputResPsw.getText().toString().trim() + "";
                if (StringUtils.isEmpty(ResetPhone)) {
                    ToastUtil.getInstance().show("请输入手机号");
                    return;
                }
                if (AccountDetectionUtils.isMobileNO110(ResetPhone)) {
                    ToastUtil.getInstance().show("手机格式不正确");
                    return;
                }
                if (StringUtils.isEmpty(ResetCode)) {
                    ToastUtil.getInstance().show("请输入验证码");
                    return;
                }
                if (!ResetCode.equals("" + sms_code)) {
                    ToastUtil.getInstance().show("验证码不正确");
                    return;
                }
                if (StringUtils.isEmpty(Psw)) {
                    ToastUtil.getInstance().show("请输入密码");
                    return;
                }
                if (Psw.length() < 6 || Psw.length() > 8) {
                    ToastUtil.getInstance().show("密码格式为6-8位");
                    return;
                }
                if (StringUtils.isEmpty(ResPsw)) {
                    ToastUtil.getInstance().show("请输入确认密码");
                    return;
                }
                if (ResPsw.length() < 6 || ResPsw.length() > 8) {
                    ToastUtil.getInstance().show("确认密码格式为6-8位");
                    return;
                }
                chackPsi();
                resetPsw(ResetPhone, ResetCode, ResPsw, imei);
                break;
        }
    }

    private void resetPsw(String resetPhone, String resetCode, String resPsw, String imei) {
        UserInfoEntity user = new UserInfoEntity();
        user.setData(new UserInfoEntity.DataBean());
        user.getData().setPhone(resetPhone);
        user.getData().setPassword(resPsw);
        user.getData().setDeviceNo(imei);
        HttpManager.resSetPsw(user, resetCode, this, new BaseCallback<ReSetPswEntity>() {
            @Override
            public void onLogicSuccess(ReSetPswEntity uploadEntity) {
                super.onLogicSuccess(uploadEntity);
                ToastUtil.getInstance().show("重置密码完成");
                finish();
            }

            @Override
            public void onLogicFailure(ReSetPswEntity uploadEntity) {
                super.onLogicFailure(uploadEntity);
                ToastUtil.getInstance().show(uploadEntity.ret_msg);
            }
        });
    }

    public void getCode(String registerPhones) {
        HttpManager.getCode(registerPhones, this,"1", new BaseCallback<SmsCodeEntity>() {


            @Override
            public void onLogicFailure(SmsCodeEntity base) {
                super.onLogicFailure(base);
                ToastUtil.getInstance().show(base.ret_msg);
            }

            @Override
            public void onLogicSuccess(SmsCodeEntity base) {
                super.onLogicSuccess(base);
                sms_code = base.getData();
                mTimeCountUtils.start();
            }
        });
    }

    private void chackPsi() {
        //是6.0    没有授权
        if (CheckPermissionsUtils.isSDK_M()) {
            if (!CheckPermissionsUtils.CheckPermissions(this, Manifest.permission.READ_PHONE_STATE)) {
                CheckPermissionsUtils.requestPsi(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);

            }
            if (CheckPermissionsUtils.CheckPermissions(this, Manifest.permission.READ_PHONE_STATE))
                getiemi();

        }
        if (!CheckPermissionsUtils.isSDK_M()) getiemi();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            int checkCallPhonePermission =
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
            if (checkCallPhonePermission != PackageManager.PERMISSION_GRANTED) {
                ToastUtil.getInstance().show("请在手机管家授予唯一标识权限");
                return;
            }
            getiemi();


        }

    }

    public void getiemi() {

        imei =  AppInfo.getModelId(this);
    }

    @Override
    protected boolean isApplyReceiver() {
        return true;
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_re_set_psw;
    }
}
