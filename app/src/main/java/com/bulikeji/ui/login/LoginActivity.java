package com.bulikeji.ui.login;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bulikeji.App;
import com.bulikeji.MainActivity;
import com.bulikeji.R;
import com.bulikeji.acp.Acp;
import com.bulikeji.acp.AcpOptions;
import com.bulikeji.base.BaseActivity;
import com.bulikeji.com.lzy.okhttputils.utils.log.KLog;
import com.bulikeji.net.HttpManager;
import com.bulikeji.net.callback.BaseCallback;
import com.bulikeji.ui.entity.RegisterEntity;
import com.bulikeji.ui.entity.SmsCodeEntity;
import com.bulikeji.ui.entity.UserInfoEntity;
import com.bulikeji.ui.setting.ReSetPswActivity;
import com.bulikeji.utils.AccountDetectionUtils;
import com.bulikeji.utils.AppInfo;
import com.bulikeji.utils.CheckPermissionsUtils;
import com.bulikeji.utils.PhoneInfo;
import com.bulikeji.utils.StringUtils;
import com.bulikeji.utils.TimeCountUtils;
import com.bulikeji.utils.ToastUtil;
import com.bulikeji.utils.UserInfoSharedPreUtils;
import com.hyphenate.EMCallBack;
import com.hyphenate.EMError;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chatui.DemoHelper;
import com.hyphenate.exceptions.HyphenateException;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

import java.util.HashMap;
import java.util.List;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.PlatformDb;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.wechat.friends.Wechat;


/**
 * Created by Administrator on 2017/3/18 0018.
 */
public class LoginActivity extends BaseActivity implements View.OnClickListener, PlatformActionListener {
    private android.widget.TextView register;
    private android.widget.TextView login;
    private android.widget.ImageView ivRegister;
    private android.widget.ImageView ivLogin;

    private RelativeLayout loginParent;
    private RelativeLayout rlLoginInfo;
    private EditText inputPhone;
    private EditText inputPsw;
    private Button loginClick;
    private TextView resPsw;


    private RelativeLayout registerParent;
    private RelativeLayout rlRegisterInfo;
    private EditText inputRegisterPhone;
    private EditText inputCode;
    private Button register_send_code;
    private Button registerClick;
    private String sms_code = "";
    private TimeCountUtils mTimeCountUtils;
    private String imei = "";
    private RelativeLayout pswParent;
    private EditText register_input_psw;
    private Button registerCommit;
    private String registerPhone;
    private String registerCode;
    private long lastTime;


    public static void start(Context Context) {
        Context.startActivity(new Intent(Context, LoginActivity.class));

    }

    @Override
    protected void initData() {


    }

    @Override
    protected void setListener() {
        register.setOnClickListener(this);
        login.setOnClickListener(this);
        loginClick.setOnClickListener(this);
        registerClick.setOnClickListener(this);
        register_send_code.setOnClickListener(this);
        resPsw.setOnClickListener(this);
        registerCommit.setOnClickListener(this);

        mTimeCountUtils = new TimeCountUtils(this, register_send_code, 60 * 1000, 1 * 1000);

    }

    @Override
    protected void initTitle() {

    }

    @Override
    protected void initView() {

        register = (TextView) findViewById(R.id.register);
        login = (TextView) findViewById(R.id.login);
        ivRegister = (ImageView) findViewById(R.id.iv_register);
        ivLogin = (ImageView) findViewById(R.id.iv_login);
        login.setSelected(true);
        register.setSelected(false);
        //登录


        loginParent = (RelativeLayout) findViewById(R.id.login_parent);
        rlLoginInfo = (RelativeLayout) findViewById(R.id.rl_login_info);
        inputPhone = (EditText) findViewById(R.id.input_phone);
        inputPsw = (EditText) findViewById(R.id.input_psw);
        loginClick = (Button) findViewById(R.id.login_click);
        resPsw = (TextView) findViewById(R.id.res_psw);
        //register


        registerParent = (RelativeLayout) findViewById(R.id.register_parent);
        rlRegisterInfo = (RelativeLayout) findViewById(R.id.rl_register_info);
        inputRegisterPhone = (EditText) findViewById(R.id.input_register_phone);
        inputCode = (EditText) findViewById(R.id.input_code);
        register_send_code = (Button) findViewById(R.id.login_code_click);
        registerClick = (Button) findViewById(R.id.register_click);
//密码


        pswParent = (RelativeLayout) findViewById(R.id.psw_parent);
        register_input_psw = (EditText) findViewById(R.id.register_input_psw);
        registerCommit = (Button) findViewById(R.id.register_commit);

        ShareSDK.initSDK(this);
        api = WXAPIFactory.createWXAPI(this, "wx9a8220001aa6c5c9");
    }

    private IWXAPI api;


    public void onclick(View View) {
        if (!(api.getWXAppSupportAPI()
                >= com.tencent.mm.sdk.constants.Build.PAY_SUPPORTED_SDK_INT)) {
            Toast.makeText(this, "未安装微信", Toast.LENGTH_SHORT).show();
            return;
        }

        Platform wechat = ShareSDK.getPlatform(Wechat.NAME);
        wechat.setPlatformActionListener(this);
        wechat.SSOSetting(false);
        wechat.showUser(null);
    }

    Platform mPlatform;

    @Override
    public void onComplete(Platform platform, int action, HashMap<String, Object> hashMap) {
        if (action == Platform.ACTION_USER_INFOR) {

            PlatformDb platDB = platform.getDb();//获取数平台数据DB
            mPlatform = platform;
            String openId = platDB.getUserId();
            String Headimgurl = platDB.getUserIcon();
            String NickName = platDB.getUserName();


        }
    }

    @Override
    public void onError(Platform platform, int i, Throwable throwable) {

    }

    @Override
    public void onCancel(Platform platform, int i) {

    }

    @Override
    protected boolean isApplyReceiver() {
        return true;
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_login;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.register:
                register.setSelected(true);
                login.setSelected(false);
                ivLogin.setVisibility(View.INVISIBLE);
                ivRegister.setVisibility(View.VISIBLE);
                loginParent.setVisibility(View.GONE);
                pswParent.setVisibility(View.GONE);
                registerParent.setVisibility(View.VISIBLE);
                break;
            case R.id.login:
                register.setSelected(false);
                login.setSelected(true);
                ivLogin.setVisibility(View.VISIBLE);
                ivRegister.setVisibility(View.INVISIBLE);
                loginParent.setVisibility(View.VISIBLE);
                registerParent.setVisibility(View.GONE);
                pswParent.setVisibility(View.GONE);
                break;
            case R.id.login_click:
                String login_phone = inputPhone.getText().toString().trim() + "";
                String login_psw = inputPsw.getText().toString().trim() + "";
                if (StringUtils.isEmpty(login_phone)) {
                    ToastUtil.getInstance().show("请输入手机号");
                    return;
                }
                if (AccountDetectionUtils.isMobileNO110(login_phone)) {
                    ToastUtil.getInstance().show("手机格式不正确");
                    return;
                }
                if (StringUtils.isEmpty(login_psw)) {
                    ToastUtil.getInstance().show("请输入密码");
                    return;
                }
                if (login_psw.length() < 6 || login_psw.length() > 8) {
                    ToastUtil.getInstance().show("密码格式为6-8位");
                    return;
                }
                chackPsi();
                UserInfoEntity base = new UserInfoEntity();
                UserInfoEntity.DataBean data = new UserInfoEntity.DataBean();
                data.setAge(44);
                data.setId(44);
                data.setPhone(login_phone);
                data.setPassword(login_psw);
                data.setSex("女");
                data.setSignature("aasasa");
                data.setThumb("http://img002.21cnimg.com/photos/she_0/20141113/c100-0-0-440-540_r0/9E1E9E37EE0E254A94E8B719FDB51F74.jpg");
                data.setToken("adsdasdas"+login_phone);
                base.setData(data);

                loginEM(login_phone, login_psw, base);
                break;
            case R.id.res_psw:
                ReSetPswActivity.start(this);
                break;
            case R.id.login_code_click:
                String RegisterPhones = inputRegisterPhone.getText().toString().trim() + "";
                if (StringUtils.isEmpty(RegisterPhones)) {
                    ToastUtil.getInstance().show("请输入手机号");
                    return;
                }
                if (AccountDetectionUtils.isMobileNO110(RegisterPhones)) {
                    ToastUtil.getInstance().show("手机格式不正确");
                    return;
                }
                getCode(RegisterPhones);
                break;
            case R.id.register_click:
                registerPhone = inputRegisterPhone.getText().toString().trim() + "";
                registerCode = inputCode.getText().toString().trim() + "";
                if (StringUtils.isEmpty(registerPhone)) {
                    ToastUtil.getInstance().show("请输入手机号");
                    return;
                }
                if (AccountDetectionUtils.isMobileNO110(registerPhone)) {
                    ToastUtil.getInstance().show("手机格式不正确");
                    return;
                }
                if (StringUtils.isEmpty(registerCode)) {
                    ToastUtil.getInstance().show("请输入验证码");
                    return;
                }
                if (!registerCode.equals("" + sms_code)) {
                    ToastUtil.getInstance().show("验证码不正确");
                    return;
                }
                showInputPsw();
                break;
            case R.id.register_commit:
                final String register_psw = register_input_psw.getText().toString().trim();
                if (StringUtils.isEmpty(register_psw)) {
                    ToastUtil.getInstance().show("请输入密码");
                    return;
                }
                if (register_psw.length() < 6 || register_psw.length() > 8) {
                    ToastUtil.getInstance().show("密码格式为6-8位");
                    return;
                }
                chackPsi();

                UserInfoEntity registerEntity = new UserInfoEntity();
                UserInfoEntity.DataBean datas = new UserInfoEntity.DataBean();
                datas.setAge(44);
                datas.setId(44);
                datas.setPhone(registerPhone);
                datas.setPassword(register_psw);
                datas.setSex("女");
                datas.setSignature("aasasa");
                datas.setToken("asdasdasdsa" + registerPhone);
                datas.setThumb("http://img002.21cnimg.com/photos/she_0/20141113/c100-0-0-440-540_r0/9E1E9E37EE0E254A94E8B719FDB51F74.jpg");
                registerEntity.setData(datas);

                registerEM(registerPhone, register_psw, registerEntity);
                break;

        }

    }

    private void chackPsi() {
        //是6.0    没有授权
        if (CheckPermissionsUtils.isSDK_M()) {
            if (!CheckPermissionsUtils.CheckPermissions(this, Manifest.permission.READ_PHONE_STATE)) {
                CheckPermissionsUtils.requestPsi(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);

            }
            if (CheckPermissionsUtils.CheckPermissions(this, Manifest.permission.READ_PHONE_STATE))
                getiemi();

        }
        if (!CheckPermissionsUtils.isSDK_M()) getiemi();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            int checkCallPhonePermission =
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
            if (checkCallPhonePermission != PackageManager.PERMISSION_GRANTED) {
                ToastUtil.getInstance().show("请在手机管家授予唯一标识权限");
                return;
            }
            getiemi();


        }

    }

    private void showInputPsw() {
        pswParent.setVisibility(View.VISIBLE);
        loginParent.setVisibility(View.GONE);
        registerParent.setVisibility(View.GONE);
    }


    private void loginEM(String login_phone, String login_psw, final UserInfoEntity base) {
        EMClient.getInstance().login(login_phone, login_psw, new EMCallBack() {

            @Override
            public void onSuccess() {
                KLog.d("loginEM: onSuccess");
                UserInfoSharedPreUtils.getInstance().put(LoginActivity.this, base);
                ToastUtil.getInstance().show("环信登录成功");

                // ** manually load all local groups and conversation
                EMClient.getInstance().groupManager().loadAllGroups();
                EMClient.getInstance().chatManager().loadAllConversations();

                // update current user's display name for APNs
                boolean updatenick = EMClient.getInstance().pushManager().updatePushNickname(
                        App.currentUserNick.trim());
                if (!updatenick) {
                    KLog.d("update current user nick fail");
                }


                // get user's info (this should be get from App's server or 3rd party service)
                DemoHelper.getInstance().getUserProfileManager().asyncGetCurrentUserInfo();

                MainActivity.start(LoginActivity.this, base);
                finish();
            }

            @Override
            public void onProgress(int progress, String status) {
                KLog.d("loginEM: onProgress");
            }

            @Override
            public void onError(final int code, final String message) {

                runOnUiThread(new Runnable() {
                    public void run() {
                        ToastUtil.getInstance().show(message + "==onError");
                        MainActivity.start(LoginActivity.this, base);
                        finish();
                    }
                });

            }
        });
    }


    public void getCode(String registerPhones) {
        sms_code = "123456";
        mTimeCountUtils.start();
    }

    public boolean ispermitfinish = true;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            KLog.d("onKeyDown", ispermitfinish);
            if (!ispermitfinish) return true;
            if (System.currentTimeMillis() - lastTime < 2000) {
                UserInfoSharedPreUtils.getInstance().clear(this);
                return super.onKeyDown(keyCode, event);
            } else {
                lastTime = System.currentTimeMillis();
                ToastUtil.getInstance().show("再按一次退出应用");
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    public void getiemi() {
        imei = AppInfo.getModelId(this);

    }

    public void registerEM(final String username, final String pwd, final UserInfoEntity base) {
        new Thread(new Runnable() {
            public void run() {
                try {
                    // call method in SDK
                    EMClient.getInstance().createAccount(username, pwd);
                    runOnUiThread(new Runnable() {
                        public void run() {

                            // save current user
                            DemoHelper.getInstance().setCurrentUserName(username);

                            MainActivity.start(LoginActivity.this, base);
                            finish();
                        }
                    });
                } catch (final HyphenateException e) {
                    runOnUiThread(new Runnable() {
                        public void run() {

                            int errorCode = e.getErrorCode();
                            if (errorCode == EMError.NETWORK_ERROR) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_anomalies), Toast.LENGTH_SHORT).show();
                            } else if (errorCode == EMError.USER_ALREADY_EXIST) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.User_already_exists), Toast.LENGTH_SHORT).show();
                            } else if (errorCode == EMError.USER_AUTHENTICATION_FAILED) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.registration_failed_without_permission), Toast.LENGTH_SHORT).show();
                            } else if (errorCode == EMError.USER_ILLEGAL_ARGUMENT) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.illegal_user_name), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.Registration_failed), Toast.LENGTH_SHORT).show();
                            }
                            ToastUtil.getInstance().show("注册EM ERROR");
                            DemoHelper.getInstance().setCurrentUserName(username);

                            MainActivity.start(LoginActivity.this, base);
                            finish();
                        }
                    });
                }
            }
        }).start();
    }
}
