package com.bulikeji.ui.login;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bulikeji.App;
import com.bulikeji.MainActivity;
import com.bulikeji.R;
import com.bulikeji.acp.Acp;
import com.bulikeji.acp.AcpOptions;
import com.bulikeji.base.BaseActivity;
import com.bulikeji.com.lzy.okhttputils.utils.log.KLog;
import com.bulikeji.net.HttpManager;
import com.bulikeji.net.callback.BaseCallback;

import com.bulikeji.receiver.ReceiverUtil;
import com.bulikeji.ui.entity.RegisterEntity;
import com.bulikeji.ui.entity.UploadEntity;
import com.bulikeji.ui.entity.UserInfoEntity;
import com.bulikeji.ui.setting.EditUserInfoActivity;
import com.bulikeji.utils.ActionSheetDialog;
import com.bulikeji.utils.AppInfo;
import com.bulikeji.utils.CheckPermissionsUtils;
import com.bulikeji.utils.DrawableUtils;
import com.bulikeji.utils.PhoneInfo;
import com.bulikeji.utils.PhotoUtil;
import com.bulikeji.utils.StringUtils;
import com.bulikeji.utils.ToastUtil;
import com.bulikeji.utils.UserInfoSharedPreUtils;
import com.bulikeji.utils.de.hdodenhof.circleimageview.CircleImageView;
import com.bulikeji.utils.photo.PhotosIntent;
import com.bulikeji.utils.photov2.CropUtils;
import com.bulikeji.utils.photov2.DialogPermission;
import com.bulikeji.utils.photov2.FileUtil;
import com.bulikeji.utils.photov2.PermissionUtil;
import com.bulikeji.utils.photov2.SharedPreferenceMark;
import com.bulikeji.widget.TitleBarView;
import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chatui.DemoHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import static com.bulikeji.utils.Constants.ACTION_CAMERA;
import static com.bulikeji.utils.Constants.ACTION_CROP;
import static com.bulikeji.utils.Constants.ACTION_LOCATION;
import static com.darsh.multipleimageselect.helpers.Constants.REQUEST_CODE;

public class RegisterActivity extends BaseActivity implements View.OnClickListener {


    private TitleBarView title;
    private com.bulikeji.utils.de.hdodenhof.circleimageview.CircleImageView userIcon;
    private android.widget.TextView selectIcon;
    private android.widget.EditText inputRegisterName;
    private android.widget.Button btW;
    private android.widget.Button btM;
    private android.widget.EditText inputRegisterAge;
    private android.widget.EditText inputRegisterSchool;
    private android.widget.Button registerClick;
    private String path = "";
    private String name = "";
    private String age = "";
    private String school = "";
    private String sex = "1";
    private String phone = "";
    private boolean sex1;
    private String coverPath = "";
    private String imsi = "";
    private String psw = "";
    private long lastTime;
    private int acp_code = 0;//1  读写  2相机  3IEMI
    private boolean is_change = false;
    private double isWRcode;
    private boolean is_select_photo;
    private File file;
    private Uri uri;
    private Bitmap bitmap;


    public static void start(Context Context, RegisterEntity registerEntity,String psw) {
        Intent Intent = new Intent(Context, RegisterActivity.class);
        UserInfoSharedPreUtils.getInstance().put(Context, registerEntity);
        Intent.putExtra("psw",psw);
        Context.startActivity(Intent);
    }

    public static void start(Context Context) {
        Intent Intent = new Intent(Context, RegisterActivity.class);
        Context.startActivity(Intent);
    }

    @Override
    protected void initData() {
        phone = getIntent().getStringExtra("phone"); psw = getIntent().getStringExtra("psw");


    }

    @Override
    protected void setListener() {
        btM.setOnClickListener(this);
        btW.setOnClickListener(this);
        selectIcon.setOnClickListener(this);
        userIcon.setOnClickListener(this);
        registerClick.setOnClickListener(this);
    }

    @Override
    protected void initTitle() {
        title.setTitle("注册资料");

    }

    @Override
    protected void initView() {


        userIcon = (CircleImageView) findViewById(R.id.user_icon);
        selectIcon = (TextView) findViewById(R.id.select_icon);
        inputRegisterName = (EditText) findViewById(R.id.input_register_name);
        btW = (Button) findViewById(R.id.bt_w);
        btM = (Button) findViewById(R.id.bt_m);
        inputRegisterAge = (EditText) findViewById(R.id.input_register_age);
        inputRegisterSchool = (EditText) findViewById(R.id.input_register_school);
        registerClick = (Button) findViewById(R.id.register_click);
        title = (TitleBarView) findViewById(R.id.titlew);
        title.hideBack();
        initStyle(sex1);
        init();

    }

    private static final int REQUEST_CODE_TAKE_PHOTO = 1;
    private static final int REQUEST_CODE_ALBUM = 2;
    private static final int REQUEST_CODE_CROUP_PHOTO = 3;
    private void init() {
        file = new File(FileUtil.getCachePath(this), "user-avatar.jpg");
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            uri = Uri.fromFile(file);
        } else {
            //通过FileProvider创建一个content类型的Uri(android 7.0需要这样的方法跨应用访问)
            uri = FileProvider.getUriForFile(App.getInstance(), "com.bulikeji", file);
        }
    }
    private void initStyle(boolean b) {
        if (b) {
            btM.setSelected(false);
            btW.setSelected(true);
            btW.setTextColor(getResources().getColor(R.color.white));
            btM.setTextColor(getResources().getColor(R.color.black));

            return;
        }
        btW.setSelected(false);
        btM.setSelected(true);
        btM.setTextColor(getResources().getColor(R.color.white));
        btW.setTextColor(getResources().getColor(R.color.black));

    }

    @Override
    protected boolean isApplyReceiver() {
        return true;
    }

    @Override
    public void onEvent(Intent intent) {
        super.onEvent(intent);



    }

    @Override
    protected int getContentView() {
        return R.layout.activity_register;
    }


    public void getIemi() {

        imsi = AppInfo.getModelId(this);
    }


    private void chackPsi() {
        //是6.0    没有授权
        if (CheckPermissionsUtils.isSDK_M()) {
            if (!CheckPermissionsUtils.CheckPermissions(this, Manifest.permission.READ_PHONE_STATE)) {
                CheckPermissionsUtils.requestPsi(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 3);

            }
            if (CheckPermissionsUtils.CheckPermissions(this, Manifest.permission.READ_PHONE_STATE))
                getIemi();

        }
        if (!CheckPermissionsUtils.isSDK_M()) getIemi();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.select_icon:
            case R.id.user_icon:


                new ActionSheetDialog(this).builder()
                        .setCancelable(false)
                        .setCanceledOnTouchOutside(false)
                        .addSheetItem("从相册选一张", ActionSheetDialog.SheetItemColor.Blue,
                                new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {
                                        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                                            ToastUtil.getInstance().show("SD不可用");
                                            return;
                                        }
                                        if (PermissionUtil.hasCameraPermission(RegisterActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE, PermissionUtil.REQUEST_WRITE_EXTERNAL_STORAGE)
                                                && PermissionUtil.hasCameraPermission(RegisterActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE, PermissionUtil.REQUEST_READ_EXTERNAL_STORAGE)) {

                                            choose();
                                        }

                                    }
                                })
                        .addSheetItem("拍一张相片", ActionSheetDialog.SheetItemColor.Blue,
                                new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {
                                        if (PermissionUtil.hasCameraPermission(RegisterActivity.this, Manifest.permission.CAMERA, PermissionUtil.REQUEST_SHOWCAMERA)) {
                                            take();
                                        }
                                    }
                                })
                        .show();
                break;
            case R.id.bt_m:
                sex1 = false;
                initStyle(sex1);
                break;
            case R.id.bt_w:
                sex1 = true;
                initStyle(sex1);
                break;
            case R.id.register_click:
                checkInfo();
                break;
        }

    }


    private void checkInfo() {
        String Name = inputRegisterName.getText().toString().trim() + "";
        String Age = inputRegisterAge.getText().toString().trim() + "";
        String School = inputRegisterSchool.getText().toString().trim() + "";

        if (StringUtils.isEmpty(Name)) {
            ToastUtil.getInstance().show("请输入名称");
            return;
        }
        if (Name.length() > 6 || Name.length() < 2) {
            ToastUtil.getInstance().show("名称格式为2-6位");
            return;
        }
        if (StringUtils.isEmpty(Age)) {
            ToastUtil.getInstance().show("请输入Age");
            return;
        }
        if (StringUtils.isEmpty(School)) {
            ToastUtil.getInstance().show("请输入School");
            return;
        }
        if (StringUtils.isEmpty(coverPath)) {
            ToastUtil.getInstance().show("请选择头像");
            return;
        }
        chackPsi();
        path = coverPath;
        school = School;
        age = Age;
        name = Name;
        sex = sex1 ? "女" : "男";
        final UserInfoEntity UserInfoEntity = new UserInfoEntity();
        UserInfoEntity.setData(new UserInfoEntity.DataBean());
        UserInfoEntity.getData().setName(name);
        UserInfoEntity.getData().setThumb(path);
        UserInfoEntity.getData().setAge(Integer.valueOf(age));
        UserInfoEntity.getData().setSex(sex);
        UserInfoEntity.getData().setSchool(school);
        UserInfoEntity.getData().setPhone((String) UserInfoSharedPreUtils.getInstance().getPhone(this));
        UserInfoEntity.getData().setDeviceNo(imsi);
        String TOKEN = (String) UserInfoSharedPreUtils.getInstance().getToken(this);
        UserInfoEntity.getData().setToken(TOKEN);
        UserInfoEntity.getData().setSignature("");
        is_change = true;
        HttpManager.update(UserInfoEntity, this, new BaseCallback<UserInfoEntity>() {
            @Override
            public void onLogicSuccess(UserInfoEntity userInfoEntity) {
                super.onLogicSuccess(userInfoEntity);
                UserInfoSharedPreUtils.getInstance().put(RegisterActivity.this, userInfoEntity);
                UserInfoSharedPreUtils.getInstance().setIsInputRegisterInfo(RegisterActivity.this, true);
                ToastUtil.getInstance().show("填写资料完成");
                loginEM(UserInfoEntity.getData().getPhone(), psw);

            }

            @Override
            public void onLogicFailure(UserInfoEntity userInfoEntity) {
                super.onLogicFailure(userInfoEntity);
                ToastUtil.getInstance().show(userInfoEntity.ret_msg);
            }
        });


    }

    private void loginEM(String login_phone, String login_psw) {
        EMClient.getInstance().login(login_phone, login_psw, new EMCallBack() {

            @Override
            public void onSuccess() {
                KLog.d("loginEM: onSuccess");
                ToastUtil.getInstance().show("环信登录成功");


                EMClient.getInstance().groupManager().loadAllGroups();
                EMClient.getInstance().chatManager().loadAllConversations();

                boolean updatenick = EMClient.getInstance().pushManager().updatePushNickname(
                        App.currentUserNick.trim());
                DemoHelper.getInstance().getUserProfileManager().updateCurrentUserNickName((String) UserInfoSharedPreUtils.getInstance().getName(RegisterActivity.this));
                if (bitmap!=null)
                DemoHelper.getInstance().getUserProfileManager().uploadUserAvatar(DemoHelper.Bitmap2Bytes(bitmap));
                MainActivity.start(RegisterActivity.this);
                finish();
            }

            @Override
            public void onProgress(int progress, String status) {
                KLog.d("loginEM: onProgress");
            }

            @Override
            public void onError(final int code, final String message) {

                runOnUiThread(new Runnable() {
                    public void run() {
                        ToastUtil.getInstance().show("环信登录失败");
                        MainActivity.start(RegisterActivity.this);
                        finish();
                    }
                });

            }
        });
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {

        if (requestCode == 3) {
            int checkCallPhonePermission =
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
            if (checkCallPhonePermission != PackageManager.PERMISSION_GRANTED) {
                ToastUtil.getInstance().show("请在手机管家授予唯一标识权限");
                return;
            }
            getIemi();
        }
        switch (requestCode) {

            case PermissionUtil.REQUEST_SHOWCAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    take();

                } else {
                    if (!SharedPreferenceMark.getHasShowCamera()) {
                        SharedPreferenceMark.setHasShowCamera(true);
                        new DialogPermission(this, "关闭摄像头权限影响扫描功能,请去管家->权限管理，设置权限");

                    } else {
                        Toast.makeText(this, "未获取摄像头权限", Toast.LENGTH_SHORT)
                                .show();
                    }
                }
                break;
            case PermissionUtil.REQUEST_READ_EXTERNAL_STORAGE:
            case PermissionUtil.REQUEST_WRITE_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    choose();

                } else {
                    if (!SharedPreferenceMark.getHasShowCamera()) {
                        SharedPreferenceMark.setHasShowExtralStoreage(true);
                        new DialogPermission(this, "关闭读写权限影响相册功能,请去管家->权限管理，设置权限");

                    } else {
                        Toast.makeText(this, "未获取读写权限", Toast.LENGTH_SHORT)
                                .show();
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != -1) {
            return;
        }
        if (requestCode == REQUEST_CODE_ALBUM && data != null) {
            Uri newUri;
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                newUri = Uri.parse("file:///" + CropUtils.getPath(this, data.getData()));
            } else {
                newUri = data.getData();
            }
            if (newUri != null) {
                startPhotoZoom(newUri);
            } else {
                Toast.makeText(this, "没有得到相册图片", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == REQUEST_CODE_TAKE_PHOTO) {
            startPhotoZoom(uri);
        } else if (requestCode == REQUEST_CODE_CROUP_PHOTO) {
            uploadAvatarFromPhoto();

        }


    }

    private void uploadAvatarFromPhoto() {

        coverPath = file.getPath();
        try {


            bitmap = BitmapFactory.decodeStream(new FileInputStream(new File(coverPath)));

            userIcon.setImageBitmap(bitmap);
            userIcon.invalidate();
            chackPsi();
            uploadIcon(new File(coverPath), imsi);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 裁剪拍照裁剪
     *
     * @param uri
     */
    public void startPhotoZoom(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra("crop", "true");// crop=true 有这句才能出来最后的裁剪页面.
        intent.putExtra("aspectX", 1);// 这两项为裁剪框的比例.
        intent.putExtra("aspectY", 1);// x:y=1:1
//        intent.putExtra("outputX", 400);//图片输出大小
//        intent.putExtra("outputY", 400);
        intent.putExtra("output", Uri.fromFile(file));
        intent.putExtra("outputFormat", "JPEG");// 返回格式
        startActivityForResult(intent, REQUEST_CODE_CROUP_PHOTO);
    }


    private void uploadIcon(File file, String iemi) {
        HttpManager.uploadIcon(file, iemi, this, new BaseCallback<UploadEntity>() {

            @Override
            public void onLogicFailure(UploadEntity uploadEntity) {
                super.onLogicFailure(uploadEntity);
                ToastUtil.getInstance().show(uploadEntity.ret_msg);
                PhotoUtil.setPortrait(userIcon, RegisterActivity.this);

            }

            @Override
            public void onLogicSuccess(UploadEntity uploadEntity) {
                super.onLogicSuccess(uploadEntity);
                coverPath = uploadEntity.getData() + "";
                if (!StringUtils.isEmpty(coverPath)) {
                    DrawableUtils.loadCircleImage(userIcon, coverPath);
                    userIcon.invalidate();
                }
            }
        });

    }


    private void choose() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_ALBUM);
    }



    private void take() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra(MediaStore.Images.Media.ORIENTATION, 0);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent, REQUEST_CODE_TAKE_PHOTO);
    }



    public boolean ispermitfinish = true;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            KLog.d("onKeyDown", ispermitfinish);
            if (!ispermitfinish) return true;
            if (System.currentTimeMillis() - lastTime < 2000) {

                return super.onKeyDown(keyCode, event);
            } else {
                lastTime = System.currentTimeMillis();
                ToastUtil.getInstance().show("再按一次退出应用");
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
