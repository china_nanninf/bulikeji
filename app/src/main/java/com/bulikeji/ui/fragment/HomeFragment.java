package com.bulikeji.ui.fragment;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bulikeji.R;
import com.bulikeji.base.BaseFragment;
import com.bulikeji.ui.login.LoginActivity;
import com.bulikeji.ui.login.RegisterActivity;
import com.bulikeji.ui.setting.EditUserInfoActivity;
import com.bulikeji.utils.DrawableUtils;
import com.bulikeji.utils.PhotoUtil;
import com.bulikeji.utils.ShareTool;
import com.bulikeji.utils.ToastUtil;
import com.bulikeji.utils.UserInfoSharedPreUtils;
import com.bulikeji.utils.de.hdodenhof.circleimageview.CircleImageView;
import com.bulikeji.widget.BottomShareDialog;
import com.bulikeji.widget.HomeTitleBarView;
import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMChatManager;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chatui.DemoHelper;
import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

import java.util.HashMap;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.tencent.qzone.QZone;

/**
 * Created by Administrator on 2017/3/18 0018.
 */
public class HomeFragment extends BaseFragment implements BottomShareDialog.OnShareListenter {
    private com.bulikeji.widget.HomeTitleBarView title;
    private android.widget.TextView bg;
    private com.bulikeji.utils.de.hdodenhof.circleimageview.CircleImageView userIcon;
    private android.widget.TextView editInfo;
    private android.widget.TextView age;
    private android.widget.TextView school;
    private android.widget.TextView sig;
    public PopupWindow popupWindow;
    private BottomShareDialog bottomShareDialog;

    @Override
    protected void initData() {
        ShareSDK.initSDK(getActivity());
        String token = (String) UserInfoSharedPreUtils.getInstance().getToken(getActivity());
        if (!"".equals(token) && token != null)
            initUserInfo();
    }


    public void initUserInfo() {
        title.setTitle((String) UserInfoSharedPreUtils.getInstance().getName(getActivity()));
        sig.setText("".equals((String) UserInfoSharedPreUtils.getInstance().getSignature(getActivity())) ? "这个人好懒~~" : (String) UserInfoSharedPreUtils.getInstance().getSignature(getActivity()));
        school.setText("学校: " + UserInfoSharedPreUtils.getInstance().getSchool(getActivity()));
        age.setText("年龄: " + UserInfoSharedPreUtils.getInstance().getAge(getActivity()));
        userIcon.setSelected(UserInfoSharedPreUtils.getInstance().getSex(getActivity()).equals("男") ? false : true);
        String thumb = (String) UserInfoSharedPreUtils.getInstance().getThumb(getActivity());
        DrawableUtils.loadCircleImage(userIcon, (String) UserInfoSharedPreUtils.getInstance().getThumb(getActivity()));

    }

    @Override
    protected void setListener() {
        editInfo.setOnClickListener(this);
        userIcon.setOnClickListener(this);
        title.setROnclick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popupWindow == null) {
                    showPopupWindown();
                    return;
                }
                if (!popupWindow.isShowing())
                    showPopupWindown();

            }
        });

    }

    private void showPopupWindown() {
        View popup_view = View.inflate(getActivity(), R.layout.popup_views, null);
        final RelativeLayout popupview = (RelativeLayout) popup_view.findViewById(R.id.popup_view);
        popupview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserInfoSharedPreUtils.getInstance().clear(getActivity());
                ToastUtil.getInstance().show("清除用户缓存，返回登陆");
                DemoHelper.getInstance().logout(false,null);
                        LoginActivity.start(getActivity());
                if (popupWindow != null) popupWindow.dismiss();
                getActivity().finish();

            }
        });
        mRootView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view != popupview && popupWindow != null) popupWindow.dismiss();
                return false;
            }
        });
        popupWindow = new PopupWindow(popup_view, RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        // 设置好参数之后再show
        int i = (int) getActivity().getResources().getDisplayMetrics().widthPixels - 50;
        popupWindow.setBackgroundDrawable(new ColorDrawable(0x00000000));
        popupWindow.setFocusable(true);
        popupWindow.setOutsideTouchable(true);
        popupWindow.showAsDropDown(title.iv_R);

    }


    @Override
    protected void initTitle() {

    }

    @Override
    protected void initView() {

        title = (HomeTitleBarView) mRootView.findViewById(R.id.title);
        bg = (TextView) mRootView.findViewById(R.id.bg);
        userIcon = (CircleImageView) mRootView.findViewById(R.id.user_icon);
        editInfo = (TextView) mRootView.findViewById(R.id.edit_info);
        age = (TextView) mRootView.findViewById(R.id.age);
        school = (TextView) mRootView.findViewById(R.id.school);
        sig = (TextView) mRootView.findViewById(R.id.sig);
        bottomShareDialog = new BottomShareDialog(getActivity(), true);
        bottomShareDialog.setOnShareListenter(this);

    }

    @Override
    protected int getContentView() {
        return R.layout.fragment_home;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.edit_info:
                EditUserInfoActivity.start(getActivity());
                break;
            case R.id.user_icon:
                if (bottomShareDialog != null) bottomShareDialog.show();

                break;

        }

    }

    public void shareQQ() {
        ShareTool shareTool = new ShareTool(getActivity());
        shareTool.setType(ShareTool.Type.TYPE_URL);
        shareTool.setTitle("我最近都在跑步，是【耐动】这个APP让我坚持下来的！");
        shareTool.setContent("Content");
        shareTool.setUrl("http://wiki.connect.qq.com/__trashed-2");
        shareTool.setChannel(ShareTool.Channel.QQ);
        shareTool.setNetImageUrl("http://www.nidoog.com/images/icon_logo.png");
        shareTool.share();
    }

    @Override
    public void shareWX_frrend() {
        shareWECHAT_MOMENTS();
    }

    public void shareWEIXIN() {
        ShareTool shareTool = new ShareTool(getActivity());
        shareTool.setType(ShareTool.Type.TYPE_URL);
        shareTool.setTitle("我最近都在跑步，是【耐动】这个APP让我坚持下来的！");
        shareTool.setContent("Content");
        shareTool.setUrl("http://wiki.connect.qq.com/__trashed-2");
        shareTool.setChannel(ShareTool.Channel.WEIXIN);
        shareTool.share();
    }

    @Override
    public void shareWeiBo() {
        shareWEIBO();
    }

    @Override
    public void shareQQZone() {
        QZone.ShareParams shareParams = new QZone.ShareParams();
        shareParams.setImageUrl("http://www.nidoog.com/images/icon_logo.png");
        shareParams.setShareType(Platform.SHARE_IMAGE);
        Platform qzone = ShareSDK.getPlatform(QZone.NAME);
        qzone.setPlatformActionListener(new PlatformActionListener() {
            @Override
            public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {

            }

            @Override
            public void onError(Platform platform, int i, Throwable throwable) {

            }

            @Override
            public void onCancel(Platform platform, int i) {

            }
        });
        qzone.share(shareParams);

    }

    @Override
    public void shareWX() {
        shareWEIXIN();

    }


    public void shareWECHAT_MOMENTS() {
        ShareTool shareTool = new ShareTool(getActivity());
        shareTool.setType(ShareTool.Type.TYPE_URL);
        shareTool.setTitle("我最近都在跑步，是【耐动】这个APP让我坚持下来的！");
        shareTool.setContent("Content");
        shareTool.setUrl("http://wiki.connect.qq.com/__trashed-2");
        shareTool.setChannel(ShareTool.Channel.WECHAT_MOMENTS);
        shareTool.setNetImageUrl("http://www.nidoog.com/images/icon_logo.png");
        shareTool.share();
    }

    public void shareWEIBO() {
        ShareTool shareTool = new ShareTool(getActivity());
        shareTool.setType(ShareTool.Type.TYPE_URL);
        shareTool.setTitle("我最近都在跑步，是【耐动】这个APP让我坚持下来的！");

        shareTool.setUrl("Content" + "\n" + "http://wiki.connect.qq.com/__trashed-2");
        shareTool.setChannel(ShareTool.Channel.WEIBO);
        shareTool.share();
    }

    public void shareSMS() {
        ShareTool shareTool = new ShareTool(getActivity());
        shareTool.setType(ShareTool.Type.TYPE_URL);
        shareTool.setTitle("我最近都在跑步，是【耐动】这个APP让我坚持下来的！");
        shareTool.setContent("Content" + "\n" + "http://wiki.connect.qq.com/__trashed-2");
        shareTool.setChannel(ShareTool.Channel.SMS);
        shareTool.share();
    }
}
