package com.bulikeji.ui.entity;

import com.google.gson.annotations.SerializedName;


public class BaseResponse<T> {
  @SerializedName("code")
  public int ret_code;
  @SerializedName("msg")
  public String ret_msg;

}
