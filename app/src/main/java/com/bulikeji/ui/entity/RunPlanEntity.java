package com.bulikeji.ui.entity;


import java.io.Serializable;

/**
 * Created by BelieveAndSunset on 2017/3/3 0003.
 * 1668126018@qq.com
 */

public class RunPlanEntity extends Base implements Serializable {

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    private String data;
}
