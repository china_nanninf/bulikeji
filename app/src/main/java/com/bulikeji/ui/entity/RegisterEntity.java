package com.bulikeji.ui.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/3/20 0020.
 */
public class RegisterEntity  extends  Base implements Serializable{
    /**
     * data : {"id":null,"name":null,"phone":"15899713855","num":null,"token":"35673186666594715899713855123456","password":"123456","sex":null,"age":null,"school":null,"signature":null,"email":null,"grade":null,"thumb":null,"createTime":null,"status":null,"deviceNo":"356731866665947"}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : null
         * name : null
         * phone : 15899713855
         * num : null
         * token : 35673186666594715899713855123456
         * password : 123456
         * sex : null
         * age : null
         * school : null
         * signature : null
         * email : null
         * grade : null
         * thumb : null
         * createTime : null
         * status : null
         * deviceNo : 356731866665947
         */

        @SerializedName("id")
        private String idX;
        private String name;
        private String phone;
        private String num;
        private String token;
        private String password;
        private String sex;
        private String age;
        private String school;
        private String signature;
        private String email;
        private Object grade;
        private String thumb;
        private Object createTime;
        private Object status;
        private String deviceNo;

        public String getIdX() {
            return idX;
        }

        public void setIdX(String idX) {
            this.idX = idX;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getNum() {
            return num;
        }

        public void setNum(String num) {
            this.num = num;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getSchool() {
            return school;
        }

        public void setSchool(String school) {
            this.school = school;
        }

        public String getSignature() {
            return signature;
        }

        public void setSignature(String signature) {
            this.signature = signature;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Object getGrade() {
            return grade;
        }

        public void setGrade(Object grade) {
            this.grade = grade;
        }

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        public Object getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Object createTime) {
            this.createTime = createTime;
        }

        public Object getStatus() {
            return status;
        }

        public void setStatus(Object status) {
            this.status = status;
        }

        public String getDeviceNo() {
            return deviceNo;
        }

        public void setDeviceNo(String deviceNo) {
            this.deviceNo = deviceNo;
        }
    }
}
