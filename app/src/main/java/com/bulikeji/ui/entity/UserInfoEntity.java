package com.bulikeji.ui.entity;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/3/18 0018.
 */
public class UserInfoEntity extends Base implements Serializable {


    /**
     * code : 200
     * msg : 登录成功
     * data : {"page":1,"rows":10,"id":null,"name":"张三","phone":"13163261066","num":null,"token":"1111111113163261066888888","password":"88888888","sex":"男","age":22,"school":"清华大学","signature":null,"email":null,"grade":null,"thumb":"http://img002.21cnimg.com/photos/she_0/20141113/c100-0-0-440-540_r0/9E1E9E37EE0E254A94E8B719FDB51F74.jpg","createTime":null,"status":null,"deviceNo":"11111111","code":null}
     */

    private DataBean data = new DataBean();


    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * page : 1
         * rows : 10
         * id : null
         * name : 张三
         * phone : 13163261066
         * num : null
         * token : 1111111113163261066888888
         * password : 88888888
         * sex : 男
         * age : 22
         * school : 清华大学
         * signature : null
         * email : null
         * grade : null
         * thumb : http://img002.21cnimg.com/photos/she_0/20141113/c100-0-0-440-540_r0/9E1E9E37EE0E254A94E8B719FDB51F74.jpg
         * createTime : null
         * status : null
         * deviceNo : 11111111
         * code : null
         */

        private int page;
        private int rows;
        private Object id;
        private String name;
        private String phone;
        private Object num;
        private String token;
        private String password;
        private String sex;
        private int age;
        private String school;
        private Object signature;
        private Object email;
        private Object grade;
        private String thumb;
        private Object createTime;
        private Object status;
        private String deviceNo;
        private Object code;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getRows() {
            return rows;
        }

        public void setRows(int rows) {
            this.rows = rows;
        }

        public Object getId() {
            return id;
        }

        public void setId(Object id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public Object getNum() {
            return num;
        }

        public void setNum(Object num) {
            this.num = num;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public String getSchool() {
            return school;
        }

        public void setSchool(String school) {
            this.school = school;
        }

        public Object getSignature() {
            return signature;
        }

        public void setSignature(Object signature) {
            this.signature = signature;
        }

        public Object getEmail() {
            return email;
        }

        public void setEmail(Object email) {
            this.email = email;
        }

        public Object getGrade() {
            return grade;
        }

        public void setGrade(Object grade) {
            this.grade = grade;
        }

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        public Object getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Object createTime) {
            this.createTime = createTime;
        }

        public Object getStatus() {
            return status;
        }

        public void setStatus(Object status) {
            this.status = status;
        }

        public String getDeviceNo() {
            return deviceNo;
        }

        public void setDeviceNo(String deviceNo) {
            this.deviceNo = deviceNo;
        }

        public Object getCode() {
            return code;
        }

        public void setCode(Object code) {
            this.code = code;
        }
    }
}
