package com.bulikeji.ui.entity;

import com.google.gson.annotations.SerializedName;

import com.orm.SugarRecord;

public class Base extends SugarRecord {

  @SerializedName("code")
  public int ret_code;
  @SerializedName("msg")
  public String ret_msg;

  @Override
  public String toString() {
    return super.toString();
  }
}
