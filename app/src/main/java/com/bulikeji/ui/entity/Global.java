package com.bulikeji.ui.entity;


public class Global {
  public static String Sign = "";
  public static String OpenId = "";
  public static String Name = "";
  public static String Mobile = "";
  public static String QQ = "";
  public static String WeiBo = "";
  public static String WeiXin = "";
  public static String Icon = "";
  public static boolean Enabled = false;
  public static String Money = "";
  public static String FreezeMoney = "";
  public static String PunishMoney = "";
  public static int RunNum;
  public static int SendPlanNum;
  public static int ReceivePlanNum;
  public static String Mileage = "";
  public static String GetMoney = "";
  public static String BreakMoney = "";
  public static String DepositMoney = "";
  public static long UseTime;
  public static int ENVELOPES_TYPE_SINGLE = 1;
  public static int ENVELOPES_TYPE_MASS = 2;
  public static String CurrentServerTime = "";
  public static long CurrentServerTimeMillis = 0;
}
