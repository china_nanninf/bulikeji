package com.bulikeji.ui.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Believe on 2017/3/24 0024.
 * 1668126018@qq.com
 */
public class ReSetPswEntity extends  Base {

    /**
     * data : {"id":3,"name":"我是你大哥","phone":"15899713855","num":null,"token":"ca7ab7a5189bfbd446a510bf535f05cf","password":null,"sex":"男","age":19,"school":"重庆大学","signature":"我就是我，不一样的烟火","email":null,"grade":null,"thumb":"http://119.23.10.65:8080/live-android/images/http://119.23.10.65:8080/live-android/images/20170324/1490319549934.png","createTime":null,"status":null,"deviceNo":"null"}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 3
         * name : 我是你大哥
         * phone : 15899713855
         * num : null
         * token : ca7ab7a5189bfbd446a510bf535f05cf
         * password : null
         * sex : 男
         * age : 19
         * school : 重庆大学
         * signature : 我就是我，不一样的烟火
         * email : null
         * grade : null
         * thumb : http://119.23.10.65:8080/live-android/images/http://119.23.10.65:8080/live-android/images/20170324/1490319549934.png
         * createTime : null
         * status : null
         * deviceNo : null
         */

        @SerializedName("id")
        private int idX;
        private String name;
        private String phone;
        private Object num;
        private String token;
        private Object password;
        private String sex;
        private int age;
        private String school;
        private String signature;
        private Object email;
        private Object grade;
        private String thumb;
        private Object createTime;
        private Object status;
        private String deviceNo;

        public int getIdX() {
            return idX;
        }

        public void setIdX(int idX) {
            this.idX = idX;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public Object getNum() {
            return num;
        }

        public void setNum(Object num) {
            this.num = num;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public Object getPassword() {
            return password;
        }

        public void setPassword(Object password) {
            this.password = password;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public String getSchool() {
            return school;
        }

        public void setSchool(String school) {
            this.school = school;
        }

        public String getSignature() {
            return signature;
        }

        public void setSignature(String signature) {
            this.signature = signature;
        }

        public Object getEmail() {
            return email;
        }

        public void setEmail(Object email) {
            this.email = email;
        }

        public Object getGrade() {
            return grade;
        }

        public void setGrade(Object grade) {
            this.grade = grade;
        }

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        public Object getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Object createTime) {
            this.createTime = createTime;
        }

        public Object getStatus() {
            return status;
        }

        public void setStatus(Object status) {
            this.status = status;
        }

        public String getDeviceNo() {
            return deviceNo;
        }

        public void setDeviceNo(String deviceNo) {
            this.deviceNo = deviceNo;
        }
    }
}
